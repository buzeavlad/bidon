/**
 * 
 */
package criteria;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import model.Auction;
import model.AuctionParticipant;
import model.AuctionParticipant_;
import model.Auction_;
import model.Bid;
import model.Category;
import model.Category_;
import model.Product;
import model.Product_;
import daos.BidDAO;
import daos.BidDAOLocal;
import dto.AuctionListDTO;

/**
 * @author Ildik�
 *
 */
@Stateless
public class SearchByCriteria implements SearchByCriteriaRemote{
	
	@PersistenceContext
	EntityManager em;
	
	@EJB
	private BidDAOLocal bidDAOLocal;
	
	public void setBidDAOLocal(BidDAOLocal bidDAOLocal) {
		this.bidDAOLocal = bidDAOLocal;
	}

	private Logger LOGGER = Logger.getLogger(SearchByCriteria.class.getName());
	
	
	/**
	 * {@inheritDoc}
	 */
	public List<AuctionListDTO> findByCriteria(Criteria crit){

		
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Auction> query = cb.createQuery(Auction.class);
		Root<Auction> root = query.from(Auction.class);

		List<Predicate> plist = new ArrayList<Predicate>();
		
		if (crit.getSeller() != null) {
			Join<Auction, AuctionParticipant> join = root.join(Auction_.auctionparticipant);
			Predicate predicate = cb.equal(cb.lower(join.get(AuctionParticipant_.username)),
					crit.getSeller().toLowerCase());
			plist.add(predicate);
		}
		if (crit.getProductName() != null) {
			Join<Auction, Product> join = root.join(Auction_.product);
			Predicate predicate = cb.like(cb.lower(join.get(Product_.name)), "%"+crit.getProductName().toLowerCase()+"%");
			plist.add(predicate);
		}
		if (crit.getCategory() != null) {
			Join<Product, Category> join = root.join(Auction_.product).join(Product_.category);
			Predicate predicate = cb.equal(cb.lower(join.get(Category_.name)), crit.getCategory().toLowerCase());
			plist.add(predicate);
		}
		if (crit.getMinPrice() != -1 || crit.getMaxPrice() != -1){
			List<Integer> auctionIDs = this.findByPrice(crit.getMinPrice(), crit.getMaxPrice());
			if (auctionIDs.size() > 0){
				Predicate predicate = root.get(Auction_.id).in(auctionIDs);
				plist.add(predicate);
			}
		}

		Predicate runningPred = cb.equal(root.get(Auction_.status), Auction.AuctionStatus.RUNNING);
		
		if (plist.size() > 0) {
			Predicate newPredicate = null;
			switch (plist.size()){
			case 1:
				newPredicate = cb.and(plist.get(0), runningPred);
				break;
			case 2:
				newPredicate = cb.and(plist.get(0), plist.get(1), runningPred);
				break;
			case 3:
				newPredicate = cb.and(plist.get(0), plist.get(1), plist.get(2), runningPred);
				break;
			case 4:
				newPredicate = cb.and(plist.get(0), plist.get(1), plist.get(2), plist.get(3), runningPred);
				break;
			default:
				LOGGER.log(Level.WARNING, "Too many predicates in list!!!");
			}
			query.where(newPredicate);
		} else {
			query.where(runningPred);
		}
		
		TypedQuery<Auction> tquery = em.createQuery(query);
		List<Auction> temp = tquery.getResultList();
		List<AuctionListDTO> result = new ArrayList<AuctionListDTO>();
		
		for (Auction auction : temp){
			AuctionListDTO auctionDTO = auction.toListDTO();
			Bid bid = bidDAOLocal.findMaxBid(auction.getId());
			if (bid != null){
				auctionDTO.setCurrentPrice(bid.getAmount());
			} else {
				auctionDTO.setCurrentPrice(auction.getStartingPrice());
			}
			result.add(auctionDTO);
		}
		
		return result;
	}
	
	private List<Integer> findByPrice(int minPrice, int maxPrice){

		
		//select all auctionIDs
		List<Integer> auctionIds;
		
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Integer> query = cb.createQuery(Integer.class);
		Root<Auction> root = query.from(Auction.class);
		
		query.select(root.get(Auction_.id));
		
		TypedQuery<Integer> tQuery = em.createQuery(query);
		auctionIds = tQuery.getResultList();

		//find all maxBids
		List<Bid> maxBidList = new ArrayList<Bid>();

		for (Integer i : auctionIds){
			Bid bid = bidDAOLocal.findMaxBid(i);
			if (bid == null){
				continue;
			}

			maxBidList.add(bid);
		}

		
		//sort
		Collections.sort(maxBidList);
		
		//keep part that i need
		Iterator<Bid> it = maxBidList.iterator();
		while (it.hasNext()){
			Bid bid = it.next();
			if (minPrice != -1 && bid.getAmount() < minPrice){
				it.remove();
			}
			if (maxPrice != -1 && bid.getAmount() > maxPrice){
				it.remove();
			}
		}
		
		//get list of auctionIDs
		List<Integer> resultList = new ArrayList<Integer>();
		for (int i = 0; i< maxBidList.size(); i++){
			resultList.add(maxBidList.get(i).getAuction().getId());
		}
		
		return resultList;
	}
	
	public void setEm(EntityManager em) {
		this.em = em;
	}
}
