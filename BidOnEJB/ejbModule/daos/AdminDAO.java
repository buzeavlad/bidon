package daos;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import model.Admin;
import model.Admin_;
import model.User;

import dto.AdminDTO;

/**
 * Session Bean implementation class AdminDAO
 * 
 * @author Zanc Razvan
 */   
@Stateless
public class AdminDAO implements AdminDAORemote {

	@PersistenceContext
	EntityManager em;

	public void setEm(EntityManager em) {
		this.em = em;
	}

	private Logger LOGGER = Logger.getLogger(AuctionParticipantDAO.class
			.getName());

	public AdminDAO() {

	}

	@Override
	public boolean create(AdminDTO adminDTO) {

		if (adminDTO == null) {
			LOGGER.log(Level.WARNING, "The adminDTO given as parameter is null");
			return false;
		}

		try {
			Admin admin = new Admin();
			admin.setAutorityLevel(adminDTO.getAutorityLevel());
			em.persist(admin);

			LOGGER.log(Level.INFO,
					"The admin was successfully inserted in our database");
			return true;

		} catch (Exception e) {

			LOGGER.log(Level.SEVERE,
					"The admin wasn't inserted in our database ( Exception occurred )");
			e.printStackTrace();
		}
		return false;

	}

	@Override
	public boolean update(int id, AdminDTO adminDTO) {

		Admin lAdmin = em.find(Admin.class, id);

		if (lAdmin == null) {
			LOGGER.log(Level.WARNING, "The admin wasn't found !");
			return false;
		}

		else {
			lAdmin.setEmail(adminDTO.getEmail());
			lAdmin.setFirstName(adminDTO.getFirstName());
			lAdmin.setLastName(adminDTO.getLastName());
			lAdmin.setPassword(adminDTO.getPassword());
			lAdmin.setUsername(adminDTO.getUsername());
			lAdmin.setAutorityLevel(adminDTO.getAutorityLevel());

			LOGGER.log(Level.INFO, "The admin was successfully updated !");
			return true;
		}

	}

	@Override
	public boolean delete(String username) {

		if (username == null) {
			LOGGER.log(Level.WARNING, "The username given as parameter is null");
			return false;
		}
		Admin lAdmin = em.find(Admin.class, username);

		if (lAdmin == null) {
			LOGGER.log(Level.WARNING, "The admin with the username " + username
					+ " wasn't found !");

			return false;
		}

		else {

			try {
				Admin admin = em.merge(lAdmin);
				em.remove(admin);

				LOGGER.log(Level.INFO, "The admin with the username" + username
						+ " was successfully deleted");
				return true;

			} catch (Exception e) {

				LOGGER.log(Level.SEVERE, "The admin with the username "
						+ username + "wasn't deleted ( Exception occurred )");
				e.printStackTrace();

			}
			return false;
		}

	}

	@Override
	public AdminDTO findByUsername(String username) {

		if (username == null) {
			LOGGER.log(Level.WARNING, "The username given as parameter is null");
			return null;
		}

		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Admin> criteriaQuery = criteriaBuilder
				.createQuery(Admin.class);
		Root<Admin> user = criteriaQuery.from(Admin.class);
		criteriaQuery.where(criteriaBuilder.equal(user.get(Admin_.username),
				username));
		criteriaQuery.select(user);
		TypedQuery<Admin> q = em.createQuery(criteriaQuery);

		Admin temp = new Admin();
		try {
			temp = q.getSingleResult();
			AdminDTO lAdmin = new AdminDTO();
			if (temp != null) {

				lAdmin.setEmail(temp.getEmail());
				lAdmin.setFirstName(temp.getFirstName());
				lAdmin.setLastName(temp.getLastName());
				lAdmin.setPassword(temp.getPassword());
				lAdmin.setUsername(temp.getUsername());
				lAdmin.setAutorityLevel(temp.getAutorityLevel());

				LOGGER.log(Level.INFO, "The admin was successfully found !");
				return lAdmin;

			} else {

				LOGGER.log(Level.WARNING, "The admin wasn't found !");
				return null;
			}

		} catch (Exception e) {

			LOGGER.log(Level.INFO,
					"The admin wasn't found ! ( Exception occurred )");
			//e.printStackTrace();

		}
		return null;

	}

	@Override
	public int findIdByUsername(String username) {

		if (username == null) {
			LOGGER.log(Level.WARNING, "The username given as parameter is null");
			return -1;
		}

		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Admin> criteriaQuery = criteriaBuilder
				.createQuery(Admin.class);
		Root<Admin> user = criteriaQuery.from(Admin.class);
		criteriaQuery.where(criteriaBuilder.equal(user.get(Admin_.username),
				username));
		criteriaQuery.select(user);
		TypedQuery<Admin> q = em.createQuery(criteriaQuery);

		User temp = new User();
		try {
			temp = q.getSingleResult();
			if (temp != null) {

				LOGGER.log(Level.INFO,
						"The id was found ! And it's: " + temp.getId());
				return temp.getId();

			} else {

				LOGGER.log(Level.WARNING, "The admin with the username "
						+ username + " wasn't found !");
				return -1;
			}

		} catch (Exception e) {

			LOGGER.log(Level.SEVERE,
					"The id wasn't found ! ( Exception occured )");
			e.printStackTrace();

		}

		return -1;

	}

	@Override
	public List<AdminDTO> findAll() {

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Admin> cq = cb.createQuery(Admin.class);
		Root<Admin> root = cq.from(Admin.class);
		cq.select(root);

		TypedQuery<Admin> q = em.createQuery(cq);
		List<Admin> list = q.getResultList();
		List<AdminDTO> list2 = new ArrayList<>();

		for (Admin admin : list) {
			AdminDTO admin2 = new AdminDTO(admin.getEmail(),
					admin.getFirstName(), admin.getLastName(),
					admin.getPassword(), admin.getUsername(),
					admin.getAutorityLevel());
			list2.add(admin2);
		}
		return list2;
	}

}