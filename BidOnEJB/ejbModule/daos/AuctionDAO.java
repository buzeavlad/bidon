package daos;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import utils.EmailManager;
import utils.EmailMessage;
import utils.TimerSessionEJB;
import utils.TimerUtils;
import model.Auction;
import model.Auction.AuctionStatus;
import model.AuctionParticipant;
import model.AuctionParticipant_;
import model.Auction_;
import model.Bid;
import model.Bid_;
import model.Product;
import model.Review;
import model.Review_;
import dto.AuctionDTO;
import dto.AuctionListDTO;
import dto.AuctionParticipantDTO;
import dto.BidDTO;

/**
 * Session Bean implementation class AdminDAO
 * 
 * @author Zanc Razvan
 */
@Stateless
public class AuctionDAO implements AuctionDAORemote, AuctionDAOLocal {

	@PersistenceContext
	EntityManager em;

	public void setEm(EntityManager em) {
		this.em = em;
	}

	@EJB
	AuctionParticipantDAORemote auctionParticipantDAO;
	
	@EJB
	TimerSessionEJBLocal timerSessionEJB;

	
	private EmailManager emailManager;

	private EmailMessage message;
	
	private AuctionParticipantDTO auctionParticipantDTO;
	
	private Logger LOGGER = Logger.getLogger(AuctionParticipantDAO.class
			.getName());

	public AuctionDAO() {

	}

	@Override
	public Auction create(AuctionDTO auctionDTO) {

		if (auctionDTO == null) {
			LOGGER.log(Level.WARNING,
					"The auctionDTO given as parameter is null");
			return null;
		}

		try {
			Auction auction = new Auction();

			AuctionParticipant auctionPart = em.find(AuctionParticipant.class,
					auctionDTO.getIdSeller());
			auction.setAuctionparticipant(auctionPart);
			auction.setDueDate(auctionDTO.getDueDate());
			auction.setIncrementRate(auctionDTO.getIncrementRate());

			Product product = em.find(Product.class, auctionDTO.getIdProduct());
			auction.setProduct(product);

			auction.setStartingPrice(auctionDTO.getStartingPrice());
			auction.setStatus(AuctionStatus.RUNNING);
			
			em.persist(auction);
			em.flush();
			auction.getAuctionparticipant().getCreatedAuctions().add(auction);
			System.out.println(auction.getId());
			LOGGER.log(Level.INFO, "The auction was successfully created !");
			return auction;

		} catch (Exception e) {

			LOGGER.log(Level.SEVERE,
					"The auction wasn't created ( Exception occurred )");
			e.printStackTrace();
		}
		return null;

	}

	@Override
	public boolean update(int id, AuctionDTO auctionDTO) {

		Auction lAuction = em.find(Auction.class, id);

		if (lAuction == null) {
			LOGGER.log(Level.WARNING, "The auction wasn't found !");
			return false;
		}

		else {
			AuctionParticipant auctionPart = em.find(AuctionParticipant.class,
					auctionDTO.getIdSeller());
			lAuction.setAuctionparticipant(auctionPart);

			lAuction.setDueDate(auctionDTO.getDueDate());
			lAuction.setIncrementRate(auctionDTO.getIncrementRate());

			Product product = em.find(Product.class, auctionDTO.getIdProduct());
			lAuction.setProduct(product);

			lAuction.setStartingPrice(auctionDTO.getStartingPrice());
			lAuction.setStatus(Auction.AuctionStatus.RUNNING);

			LOGGER.log(Level.INFO, "The auction was successfully updated !");
			return true;
		}

	}

	

	@Override
	public List<AuctionListDTO> getCreatedAuctions(
			AuctionParticipantDTO auctPartDTO) {

		if (auctPartDTO == null)
			return new LinkedList<AuctionListDTO>();

		List<AuctionListDTO> result = new ArrayList<AuctionListDTO>();
		AuctionParticipant owner = em.find(AuctionParticipant.class,
				auctionParticipantDAO.findIdByUsername(auctPartDTO
						.getUsername()));
		for (Auction auction : owner.getCreatedAuctions()) {
			AuctionListDTO temp = auction.toListDTO();
			temp.setId(auction.getId());
			if(auction.getMaxBid()!=null)
				temp.setCurrentPrice(auction.getMaxBid().getAmount());
			else
				temp.setCurrentPrice(temp.getStartingPrice());
			if(auction.getStatus()==AuctionStatus.RUNNING)
				temp.setStatus(AuctionDTO.RUNNING);
			if(auction.getStatus()==AuctionStatus.FINISHED)
				temp.setStatus(AuctionDTO.FINISHED);
			if(auction.getStatus()==AuctionStatus.PENDING)
				temp.setStatus(AuctionDTO.PENDING);
			result.add(temp);
		}

		return result;
	}

	@Override
	public List<AuctionListDTO> getOnGoingAuction() {

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Auction> query = cb.createQuery(Auction.class);
		Root<Auction> root = query.from(Auction.class);

		query.where(cb.equal(root.get(Auction_.status),
				Auction.AuctionStatus.RUNNING));

		TypedQuery<Auction> tquery = em.createQuery(query);
		List<Auction> temp = tquery.getResultList();
		List<AuctionListDTO> result = new ArrayList<AuctionListDTO>();

		for (Auction auction : temp) {
			AuctionListDTO adto = auction.toListDTO();
			// if (auction.getMaxBid() != null)
			// adto.setCurrentPrice(auction.getMaxBid().getAmount());
			result.add(adto);
		}

		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean finishAuction(int id) {

		Auction lAuction = em.find(Auction.class, id);
		if (lAuction == null) {
			LOGGER.log(Level.WARNING, "The auction wasn't found!");
			return false;
		} else {

			lAuction.setStatus(Auction.AuctionStatus.FINISHED);

			em.persist(lAuction);
			
			LOGGER.log(Level.INFO, "The auction was successfully updated!");
			
			
			
			return true;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public AuctionDTO findById(int id) {

		Auction lAuction = em.find(Auction.class, id);
		if (lAuction == null) {
			LOGGER.log(Level.WARNING, "The auction wasn't found!");
			return null;
		} else {

			AuctionDTO temp = lAuction.toDTO();

			LOGGER.log(Level.INFO, "The auction was found!");
			return temp;
		}

	}

	/**
	 * {@inheritDoc}
	 */
	public AuctionParticipantDTO findWinner(int auctionId) {

		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Bid> criteriaQuery = criteriaBuilder
				.createQuery(Bid.class);
		Root<Bid> bid = criteriaQuery.from(Bid.class);
		
		Join<Bid, Auction> join = bid.join(Bid_.auction);
		
		Subquery<Integer> subquery = criteriaQuery.subquery(Integer.class);
		Root<Bid> from = subquery.from(Bid.class);
		Join<Bid, Auction> innerQueryJoin = from.join(Bid_.auction);
		Predicate innerPred = criteriaBuilder.equal(innerQueryJoin.get(Auction_.id), auctionId);
		Expression<Integer> max = criteriaBuilder.max(from.get(Bid_.amount));
		subquery.select(max);
		subquery.where(innerPred);
		
		
		Predicate pred = criteriaBuilder.equal(join.get(Auction_.id), auctionId);
		Predicate pred2 = criteriaBuilder.in(bid.get(Bid_.amount)).value(subquery);
		
		criteriaQuery.where(pred, pred2);
		
		TypedQuery<Bid> q = em.createQuery(criteriaQuery);
		try {
			Bid lBid = q.getSingleResult();
			AuctionParticipant lParticipant = lBid.getAuctionparticipant();
			return lParticipant.toDTO();
		}
		catch (NoResultException e) {
			return null;
		}
	}

	/**
	 * @author Vlad
	 * @param idAuction
	 * @return
	 */
	public Bid getLastBid(int idAuction) {
		String jpql = "Select b FROM Bid b where b.auction.id = :idAuc ORDER BY b.amount DESC";
		TypedQuery<Bid> query = em.createQuery(jpql, Bid.class);
		query.setParameter("idAuc", idAuction);
		query.setMaxResults(1);
		return query.getSingleResult();
	}

	public Auction findAuctionById(int idAuction) {
		Auction auction = em.find(Auction.class, idAuction);
		return auction;
	}

	@Override
	public BidDTO getLastBidDTO(int idAuction) {
		Bid lBid = getLastBid(idAuction);
		if (lBid != null) {
			BidDTO lDto = new BidDTO();
			lDto.setAmount(lBid.getAmount());
			lDto.setDate(lBid.getDate());
			lDto.setIdAuction(lBid.getAuction().getId());
			lDto.setIdBidder(lBid.getAuctionparticipant().getId());
			return lDto;
		}
		return null;
	}

	@Override
	public List<AuctionListDTO> getWonAuctions(AuctionParticipantDTO auctPartDTO) {
		if (auctPartDTO == null)
			return new LinkedList<AuctionListDTO>();

		String jpql = "Select b1 FROM Bid b1 where b1.amount = "
				+ "(SELECT max(amount) FROM Bid b2 WHERE b2.auction.id = b1.auction.id)"
				+ " and b1.auctionparticipant.username = :userN";

		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Bid> criteriaQuery = criteriaBuilder
				.createQuery(Bid.class);
		Root<Bid> root = criteriaQuery.from(Bid.class);

		Subquery<Integer> sq = criteriaQuery.subquery(Integer.class);
		Root<Bid> root2 = sq.from(Bid.class);

		Join<Bid, Auction> auction2 = root2.join(Bid_.auction);
		Join<Bid, Auction> auction = root.join(Bid_.auction);
		Join<Bid, AuctionParticipant> auctPart = root
				.join(Bid_.auctionparticipant);

		sq.select(criteriaBuilder.max(root2.get(Bid_.amount)));

		List<Predicate> predicates = new ArrayList<>();
		predicates.add(criteriaBuilder.equal(auction2.get(Auction_.id),
				auction.get(Auction_.id)));
		predicates.add(criteriaBuilder.equal(
				auctPart.get(AuctionParticipant_.username),
				auctPartDTO.getUsername()));

		sq.where(criteriaBuilder.and(predicates.toArray(new Predicate[] {})));

		criteriaQuery.select(root).where(
				criteriaBuilder.equal(root.get(Bid_.amount), sq));

		TypedQuery<Bid> tQuery = em.createQuery(criteriaQuery);
		List<Bid> bidList = tQuery.getResultList();
		List<AuctionListDTO> auctionDTOList = new LinkedList<AuctionListDTO>();
		for (Bid b : bidList) {
			AuctionListDTO temp = b.getAuction().toListDTO();
			temp.setCurrentPrice(b.getAmount());
			auctionDTOList.add(temp);
		}
		return auctionDTOList;

	}

	@Override
	public boolean cancelAuction(int id) {
		if(finishAuction(id)){
			timerSessionEJB.killTimer(id);
			emailManager = new EmailManager();
			message = new EmailMessage();
			Auction lAuction = em.find(Auction.class, id);
			emailManager.sendMail("bid090814@gmail.com", lAuction.getAuctionparticipant().getEmail(), "Your auction has been canceled.","We here by notify you that you have cancelled your auction");

			return true;
		}
			
		
		return false;
	}


	public boolean hasWonAuction(String sellerUsername, String winnerUsername) {

		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Auction> criteriaQuery = criteriaBuilder
				.createQuery(Auction.class);
		Root<Auction> root = criteriaQuery.from(Auction.class);

		Join<Auction, AuctionParticipant> auctPart = root
				.join(Auction_.auctionparticipant);

		criteriaQuery.where(criteriaBuilder.equal(
				auctPart.get(AuctionParticipant_.username), sellerUsername));

		TypedQuery<Auction> tQuery = em.createQuery(criteriaQuery);
		List<Auction> list = tQuery.getResultList();

		for (Auction a : list) {

			if (findWinner(a.getId()).getUsername().equals(winnerUsername)) {
				return true;
			}

		}

		return false;
	}
}
