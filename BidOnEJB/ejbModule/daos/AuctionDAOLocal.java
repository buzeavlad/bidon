package daos;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Remote;

import model.Auction;
import model.AuctionParticipant;
import dto.AuctionDTO;
import dto.AuctionListDTO;
import dto.AuctionParticipantDTO;

  
/**
 * 
 * Interface that will be implemented by AuctionDAO.
 * Defines basic operations for AuctionDAO
 * 
 * @author Zanc Razvan
 *
 */ 
@Local
public interface AuctionDAOLocal {
		
	public Auction create(AuctionDTO auctionDTO);
	
	public Auction findAuctionById(int idAuction);
	
	public AuctionDTO findById(int id);
	
	public boolean update(int id, AuctionDTO auctionDTO);
	
	public AuctionParticipantDTO findWinner(int auctionId);
	
}
