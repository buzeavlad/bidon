package daos;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import model.Auction.AuctionStatus;
import model.AuctionParticipant;
import model.AuctionParticipant.AuctionParticipantStatus;
import model.Auction;
import model.AuctionParticipant_;
import model.Auction_;
import model.User;
import model.User_;
import dto.AuctionParticipantDTO;
import dto.UserDTO;
import utils.EmailManager;
import utils.EmailMessage;

/**
 * Session Bean implementation class AuctionParticipantDAO
 * @author Zanc Razvan
 */
@Stateless
public class AuctionParticipantDAO implements AuctionParticipantDAORemote,AuctionParticipantDAOLocal {
	@PersistenceContext
	EntityManager em;
	public void setEm(EntityManager em) {
		this.em = em;
	}

	private Logger LOGGER = Logger.getLogger(AuctionParticipantDAO.class
			.getName());
	@EJB
	private UserDAORemote userDAO;

	/**
	 * Default constructor.
	 */
	public AuctionParticipantDAO() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean create(AuctionParticipantDTO auctionDTO) {

		if (auctionDTO == null) {
			LOGGER.log(Level.WARNING,
					"The AuctionParticipantDTO given as parameter is null");
			return false;
		}

		try {
			AuctionParticipant auction = new AuctionParticipant();
			auction.setEmail(auctionDTO.getEmail());
			auction.setFirstName(auctionDTO.getFirstName());
			auction.setLastName(auctionDTO.getLastName());
			auction.setPassword(auctionDTO.getPassword());
			auction.setUsername(auctionDTO.getUsername());
			auction.setAddress(auctionDTO.getAddress());
			auction.setBidcoins(auctionDTO.getBidcoins());
			auction.setPhone(auctionDTO.getPhone());
			auction.setStatus(AuctionParticipantStatus.PENDING);
			em.persist(auction);

			LOGGER.log(Level.INFO,
					"AuctionParticipant a fost creat cu succes !");
			return true;

		} catch (Exception e) {

			LOGGER.log(Level.SEVERE, "AuctionParticipant nu a fost creat !");
			e.printStackTrace();
		}
		return false;

	}

	@Override
	public List<AuctionParticipantDTO> allPendingAccounts() {

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<AuctionParticipant> cq = cb
				.createQuery(AuctionParticipant.class);
		Root<AuctionParticipant> root = cq.from(AuctionParticipant.class);
		
		cq.where(cb.equal(root.get(AuctionParticipant_.status),
				Auction.AuctionStatus.PENDING));
		cq.select(root);

		TypedQuery<AuctionParticipant> q = em.createQuery(cq);
		List<AuctionParticipant> list = q.getResultList();
		List<AuctionParticipantDTO> list2 = new ArrayList<>();
		for (AuctionParticipant s : list) {
			AuctionParticipantDTO s2 = new AuctionParticipantDTO(s.getEmail(),
					s.getFirstName(), s.getLastName(), s.getPassword(),
					s.getUsername(), s.getAddress(), s.getBidcoins(),
					s.getPhone(),s.isSubscribedForEmail(),s.getStatus().toString());
			list2.add(s2);
		}
		return list2;
	}

	@Override
	public boolean update(int id, AuctionParticipantDTO auctionDTO) {

		AuctionParticipant lUser = em.find(AuctionParticipant.class, id);

		if (lUser == null) {
			LOGGER.log(Level.WARNING, "The AuctionParticipant wasn't found !");
			return false;
		}

		else {

			lUser.setEmail(auctionDTO.getEmail());
			lUser.setFirstName(auctionDTO.getFirstName());
			lUser.setLastName(auctionDTO.getLastName());
			lUser.setPassword(auctionDTO.getPassword());
			lUser.setUsername(auctionDTO.getUsername());
			lUser.setAddress(auctionDTO.getAddress());
			lUser.setBidcoins(auctionDTO.getBidcoins());
			lUser.setPhone(auctionDTO.getPhone());
			if(auctionDTO.getStatus().equals(AuctionParticipantDTO.VALID))
				lUser.setStatus(AuctionParticipantStatus.VALID);
			else
				if(auctionDTO.getStatus().equals(AuctionParticipantDTO.PENDING))
					lUser.setStatus(AuctionParticipantStatus.PENDING);
				else
					lUser.setStatus(AuctionParticipantStatus.INVALID);

			LOGGER.log(Level.INFO,
					"AuctionParticipant wass successfully updated !");
			return true;
		}
	}

	@Override
	public boolean delete(String username) {

		if (username == null) {
			LOGGER.log(Level.WARNING, "The username given as parameter is null");
			return false;
		}
		int id = this.findIdByUsername(username);
		AuctionParticipant lUser = em.find(AuctionParticipant.class, id);

		if (lUser == null) {
			LOGGER.log(Level.WARNING,
					"The AuctionParticipant with the username " + username
							+ " wasn't found !");

			return false;
		}

		else {

			try {
				AuctionParticipant user1 = em.merge(lUser);
				em.remove(user1);

				LOGGER.log(Level.INFO,
						"The AuctionParticipant with the username " + username
								+ " was successfully deleted");
				return true;

			} catch (Exception e) {

				LOGGER.log(Level.SEVERE,
						"The AuctionParticipant with the username " + username
								+ " wasn't deleted ( Exception occured )");
				e.printStackTrace();
				return false;
			}
		}

	}

	@Override
	public AuctionParticipantDTO findByUsername(String username) {
		if (username == null) {
			LOGGER.log(Level.WARNING, "The username given as parameter is null");
			return null;
		}

		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<AuctionParticipant> criteriaQuery = criteriaBuilder
				.createQuery(AuctionParticipant.class);
		Root<AuctionParticipant> user = criteriaQuery
				.from(AuctionParticipant.class);
		criteriaQuery.where(criteriaBuilder.equal(
				user.get(AuctionParticipant_.username), username));
		criteriaQuery.select(user);
		TypedQuery<AuctionParticipant> q = em.createQuery(criteriaQuery);

		AuctionParticipant temp = new AuctionParticipant();
		try {
			temp = q.getSingleResult();
			AuctionParticipantDTO userOK = new AuctionParticipantDTO();
			if (temp != null) {
				userOK.setStatus(temp.getStatus().toString());
				userOK.setPassword(temp.getPassword());
				userOK.setUsername(temp.getUsername());
				userOK.setEmail(temp.getEmail());
				userOK.setFirstName(temp.getFirstName());
				userOK.setLastName(temp.getLastName());
				userOK=temp.toDTO();

				LOGGER.log(Level.INFO, "The username was successfully found !");
				return userOK;

			} else {

				LOGGER.log(Level.WARNING, "The username wasn't found !");
				return null;
			}

		} catch (Exception e) {

			LOGGER.log(Level.SEVERE,
					"The username wasn't found ! ( Exception occured )");
			e.printStackTrace();
			return null;
		}

	}

	@Override
	public AuctionParticipantDTO findByEmail(String email) {
		if (email == null) {
			LOGGER.log(Level.WARNING, "The email given as parameter is null !");
			return null;
		}

		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<AuctionParticipant> criteriaQuery = criteriaBuilder
				.createQuery(AuctionParticipant.class);
		Root<AuctionParticipant> user = criteriaQuery
				.from(AuctionParticipant.class);
		criteriaQuery.where(criteriaBuilder.equal(
				user.get(AuctionParticipant_.email), email));
		criteriaQuery.select(user);
		TypedQuery<AuctionParticipant> q = em.createQuery(criteriaQuery);

		User temp = new User();
		try {
			temp = q.getSingleResult();
			AuctionParticipantDTO userOK = new AuctionParticipantDTO();
			if (temp != null) {

				userOK.setPassword(temp.getPassword());
				userOK.setUsername(temp.getUsername());
				userOK.setEmail(temp.getEmail());
				userOK.setFirstName(temp.getFirstName());
				userOK.setLastName(temp.getLastName());

				LOGGER.log(Level.INFO, "The user was successfully found !");
				return userOK;

			} else {

				LOGGER.log(Level.WARNING, "The user wasn't found !");
				return null;
			}

		} catch (Exception e) {

			LOGGER.log(Level.SEVERE,
					"The user wasn't found ! ( Exception occured )");
			e.printStackTrace(); 
			return null;
		}

	}

	@Override
	public int findIdByUsername(String username) {
		if (username == null) {
			LOGGER.log(Level.WARNING,
					"The username given as parameter is null !");
			return -1;
		}

		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<AuctionParticipant> criteriaQuery = criteriaBuilder
				.createQuery(AuctionParticipant.class);
		Root<AuctionParticipant> user = criteriaQuery
				.from(AuctionParticipant.class);
		criteriaQuery.where(criteriaBuilder.equal(
				user.get(AuctionParticipant_.username), username));
		criteriaQuery.select(user);
		TypedQuery<AuctionParticipant> q = em.createQuery(criteriaQuery);

		User temp = new User();
		try {
			temp = q.getSingleResult();
			if (temp != null) {

				LOGGER.log(Level.INFO,
						"The id " + temp.getId()+" was succesfully found");
				return temp.getId();

			} else {

				LOGGER.log(Level.WARNING, "The username wasn't found !");
				return -1;
			}

		} catch (Exception e) {

			LOGGER.log(Level.SEVERE, "The id wasn't found ! ( Exception occured )");
			e.printStackTrace();
			return -1;
		}
	}
	
	
	/**
	 * ({@inheritDoc}
	 */
	@Override
	public AuctionParticipantDTO findByID(int id){
		if (id < 0) {
			LOGGER.log(Level.WARNING,
					"The id given as parameter is invalid.");
			return null;
		} else {
			AuctionParticipant ap = em.find(AuctionParticipant.class, id);
			if (ap != null){
				AuctionParticipantDTO auct =  ap.toDTO();
				return auct;
			}
			return null;
		}
	}

	@Override
	public void applyPentalies(String username) {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 *  Finds the user by the given email and returns the id
	 *  
	 *  @param username
	 *  @return id of the user with the given email in case if the user with the specified email was found
	 *  @return -1 if the user with the specified email was not found in the database
	 *  @author Carina Paul
	 */
	@Override
	public int findIdByEmail(String email) {
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<User> criteriaQuery = criteriaBuilder
				.createQuery(User.class);
		Root<User> user = criteriaQuery.from(User.class);
		criteriaQuery
				.where(criteriaBuilder.equal(user.get(User_.email), email));
		criteriaQuery.select(user);
		TypedQuery<User> q = em.createQuery(criteriaQuery);

		User temp = new User();
		try {
			temp = q.getSingleResult();
		
			if (temp != null) {

				System.out.println("Id'ul a fost gasit ! Este: "+temp.getId());
				return temp.getId();

			} else {

				System.out.println("Adresa de email nu a fost gasita !");
				return -1;
			}

		} catch (Exception e) {

			System.out.println("Id'ul nu a fost gasit");
			e.printStackTrace();
			return -1;
		} 
	}
	
	/**
	 *  Function which updates user password and sends an email to the user with the new password 
	 *  
	 *  @param userDTO
	 *  @param id
	 *  @return true -> if the user was successfully updated 
	 *  @return false -> if the user wasn't updated
	 */
	
	@Override
	public boolean updateAndSendMail(int id, UserDTO userDTO) {
	          return userDAO.updateAndSendMail(id,userDTO);
 
		 
		}
	
}
