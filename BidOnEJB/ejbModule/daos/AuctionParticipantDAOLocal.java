package daos;

import javax.ejb.Local;

import dto.AuctionParticipantDTO;
import dto.UserDTO;


@Local
public interface AuctionParticipantDAOLocal {
	
	/**
	 * Find the auctionParticipant with the given id;
	 * @param id
	 * @return the AuctionParticipant
	 */
	public AuctionParticipantDTO findByID(int id);

	boolean updateAndSendMail(int id, UserDTO userDTO);
	
}
