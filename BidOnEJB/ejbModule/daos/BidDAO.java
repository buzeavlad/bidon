package daos;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import utils.EmailManager;
import utils.EmailMessage;
import model.Auction;
import model.AuctionParticipant;
import model.Auction_;
import model.Bid;
import model.Bid_;
import dto.AuctionDTO;
import dto.AuctionParticipantDTO;
import dto.BidDTO;
import dto.ProductDTO;

/**
 * Session Bean implementation class AdminDAO
 * 
 * @author Zanc Razvan
 */   
@Stateless
public class BidDAO implements BidDAORemote, BidDAOLocal {

	@PersistenceContext
	EntityManager em;
	
	@EJB
	AuctionParticipantDAOLocal auctionParticipantDAOLocal;
	
	@EJB
	AuctionDAOLocal auctionDAOLocal;
	
	@EJB
	ProductDAOLocal productDAOLocal;
	
	public void setEm(EntityManager em) {
		this.em = em;
	}

	private Logger LOGGER = Logger.getLogger(AuctionParticipantDAO.class
			.getName());

	public BidDAO() {

	}
	 
	@Override
	public Bid createBid(BidDTO bidDTO) {

		if (bidDTO == null) {
			LOGGER.log(Level.WARNING, "The bidDTO given as parameter is null");
			return null;
		}

		try {
			Bid bid = new Bid();
			bid.setAmount(bidDTO.getAmount());
			Auction auction = em.find(Auction.class, bidDTO.getIdAuction());
			bid.setAuction(auction);
			AuctionParticipant auctPart = em.find(AuctionParticipant.class,
					bidDTO.getIdBidder());
			bid.setAuctionparticipant(auctPart);
			bid.setDate(bidDTO.getDate());

			em.persist(bid);
			bid.getAuction().getBids().add(bid);
			LOGGER.log(Level.INFO,
					"The bid was successfully placed to an auction");
			return bid;

		} catch (Exception e) {

			LOGGER.log(Level.SEVERE,
					"The bid wasn't placed ( Exception occurred )");
			e.printStackTrace();
		}
		return null;

	}
	
	@Override
	public boolean create(BidDTO bidDTO) {

		if (bidDTO == null) {
			LOGGER.log(Level.WARNING, "The bidDTO given as parameter is null");
			return false;
		}

		try {
			Bid bid = new Bid();
			bid.setAmount(bidDTO.getAmount());
			Auction auction = em.find(Auction.class, bidDTO.getIdAuction());
			AuctionParticipant auctPart = em.find(AuctionParticipant.class,
					bidDTO.getIdBidder());
			//Send email notification to the previous winner
			EmailManager emailM = new EmailManager();
			EmailMessage email = new EmailMessage();
			BidDTO previousBid = findMaxBidDTO(auction.getId());
			if (previousBid != null){
				AuctionParticipantDTO auctionParticipant = auctionParticipantDAOLocal.findByID(previousBid.getIdBidder());
				ProductDTO product = productDAOLocal.findByID(auction.getProduct().getId());
				String notification = email.createOutBidNotification(auctionParticipant, auction.getId(),product.getName());
				//emailM.sendMail("bid090814@gmail.com", auctPart.getEmail(), "You have been outbid!", notification);
			}
			
			//Create new bid
			bid.setAuction(auction);
			bid.setAuctionparticipant(auctPart);
			bid.setDate(bidDTO.getDate());
			bid.getAuction().addBid(bid);
			em.persist(bid);

			return true;

		} catch (Exception e) {

			LOGGER.log(Level.SEVERE,
					"The bid wasn't placed ( Exception occurred )");
			e.printStackTrace();
		}
		return false;

	}

	@Override
	public List<BidDTO> findAllBidsFromAnAuction(int idAuction) {

		if (idAuction < 0) {
			LOGGER.log(Level.WARNING,
					"The idAuction given as parameter is not valid");
			return null;
		}

		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Bid> criteriaQuery = criteriaBuilder
				.createQuery(Bid.class);
		Root<Bid> bid = criteriaQuery.from(Bid.class);
		
		Auction temp = em.find(Auction.class, idAuction);
		
		Predicate criteria = criteriaBuilder.equal(bid.get(Bid_.auction),
				temp);  

		criteriaQuery.where(criteria);
		criteriaQuery.select(bid);
		
		TypedQuery<Bid> q = em.createQuery(criteriaQuery);  

		List<Bid> list = q.getResultList();
		List<BidDTO> list2 = new ArrayList<>();

		for (Bid lBid : list) { 
			BidDTO bidDTO = new BidDTO();
			bidDTO.setAmount(lBid.getAmount());
			bidDTO.setDate(lBid.getDate());
			bidDTO.setIdAuction(lBid.getAuction().getId());
			bidDTO.setIdBidder(lBid.getAuctionparticipant().getId()); 
			list2.add(bidDTO);
		}
		return list2;

	}

	public List<Bid> getBiddersOfAuction(int auctionId){
		
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Bid> criteriaQuery = criteriaBuilder
				.createQuery(Bid.class);
		Root<Bid> bid = criteriaQuery.from(Bid.class);
		
		Join<Bid, Auction> join = bid.join(Bid_.auction);
		
		criteriaQuery.where(criteriaBuilder.equal(join.get(Auction_.id), auctionId));
		
		TypedQuery<Bid> q = em.createQuery(criteriaQuery);  

		return q.getResultList();

	}
	
	
	/**
	 * ({@inheritDoc}
	 */
	@Override
	public Bid findMaxBid(int auctionId){
		
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Bid> criteriaQuery = criteriaBuilder
				.createQuery(Bid.class);
		Root<Bid> bid = criteriaQuery.from(Bid.class);
		
		Join<Bid, Auction> join = bid.join(Bid_.auction);
		
		Subquery<Integer> subquery = criteriaQuery.subquery(Integer.class);
		Root<Bid> from = subquery.from(Bid.class);
		Join<Bid, Auction> innerQueryJoin = from.join(Bid_.auction);
		Predicate innerPred = criteriaBuilder.equal(innerQueryJoin.get(Auction_.id), auctionId);
		Expression<Integer> max = criteriaBuilder.max(from.get(Bid_.amount));
		subquery.select(max);
		subquery.where(innerPred);
		
		Predicate pred = criteriaBuilder.equal(join.get(Auction_.id), auctionId);
		Predicate pred2 = criteriaBuilder.in(bid.get(Bid_.amount)).value(subquery);
		
		criteriaQuery.where(pred, pred2);
		
		TypedQuery<Bid> q = em.createQuery(criteriaQuery);  
		
		try{
			return q.getSingleResult();
		} catch (NoResultException e){
			LOGGER.log(Level.WARNING, "BidDAO: Auction has no bids.");
			return null;
		}
		
	}
	
	/**
	 * {@inheritDoc}
	 */
	public BidDTO findMaxBidDTO(int auctionId){
		Bid bid = findMaxBid(auctionId);
		if (bid!= null){
			return bid.toDTO();
		}
		return null;
	}
	
	/**
	 * ({@inheritDoc}
	 */
	@Override
	public int getCurrentPrice(int id) {
		int amount = 0;
		
		try {
			Object am = em.createQuery("SELECT MAX(b.amount) FROM Bid b WHERE b.auction.id=:id")
					.setParameter("id",id).getSingleResult();
			if (am == null ) {
				amount = 0;
			} else {
				amount = (int) am;
			}
		} catch(NoResultException e ) {
			e.printStackTrace();
			LOGGER.log(Level.WARNING,"Query return no result.");
		}
		return amount;

	}
}
