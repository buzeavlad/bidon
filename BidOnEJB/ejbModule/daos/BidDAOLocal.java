/**
 * 
 */
package daos;

import java.util.List;

import javax.ejb.Local;

import dto.BidDTO;
import model.Bid;

/**
 * @author Ildik�
 *
 */
@Local
public interface BidDAOLocal {
	
	public Bid createBid(BidDTO bidDTO);

	public List<Bid> getBiddersOfAuction(int auctionId);
	
	
	/**
	 * finds the max bid of a given auction.
	 * @author Ildiko
	 * @param auctionId - id of auction
	 * @return max Bid, or null if no bid exists for given auction
	 */
	public Bid findMaxBid(int auctionId);
}
