package daos;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import model.Category;
import model.Category_;
import model.Review;
import dto.CategoryDTO;
import dto.ReviewDTO;

/**
 * Class that implements the basic operations for Category
 * 
 * @author Cipri
 *
 */
@Stateless
public class CategoryDAO implements CategoryDAORemote, CategoryDAOLocal {
	@PersistenceContext
	EntityManager em;

	public void setEm(EntityManager em) {
		this.em = em;
	}

	private Logger LOGGER = Logger.getLogger(CategoryDAO.class
			.getName());

	/**
	 * @inheritDoc
	 */
	@Override
	public boolean create(CategoryDTO categoryDTO) {

		if (categoryDTO == null) {
			LOGGER.log(Level.WARNING, "NULL categodyDTO !");
			return false;
		}

		try {
			Category category = new Category();
			category.setName(categoryDTO.getName());
			em.persist(category);

			LOGGER.log(Level.INFO, "SUCCES !");
			return true;

		} catch (Exception e) {

			LOGGER.log(Level.SEVERE, "FAIL Category !");
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * @inheritDoc
	 */
	@Override
	public boolean update(int id, CategoryDTO categoryDTO) {
		Category lCategory = em.find(Category.class, id);

		if (lCategory == null) {
			LOGGER.log(Level.WARNING, "NULL categodyDTO !");
			return false;
		}

		else {

			lCategory.setName(categoryDTO.getName());

			LOGGER.log(Level.INFO, "SUCCES !");
			return true;
		}

	}

	/**
	 * @inheritDoc
	 */
	@Override
	public boolean delete(String name) {

		if (name == null) {
			LOGGER.log(Level.WARNING, "NULL Parameter !");
			return false;
		}
		Category lCategory = em.find(Category.class, name);

		if (lCategory == null) {
			LOGGER.log(Level.WARNING, "NULL Returned from db !");

			return false;
		}

		else {

			try {
				Category category1 = em.merge(lCategory);
				em.remove(category1);

				LOGGER.log(Level.INFO, "SUCCES !");
				return true;

			} catch (Exception e) {

				LOGGER.log(Level.SEVERE, "FAIL Category !");
				e.printStackTrace();
				
			}
			return false;
		}

	}

	public List<CategoryDTO> findAll(){
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Category> cq = cb.createQuery(Category.class);
		Root<Category> root = cq.from(Category.class);
		cq.select(root);

		TypedQuery<Category> q = em.createQuery(cq);
		List<Category> list = q.getResultList();
		List<CategoryDTO> list2 = new ArrayList<>();
		for (Category s : list) {

			CategoryDTO s2 = new CategoryDTO();
			s2.setName(s.getName());
			list2.add(s2);
		}
		return list2;
	}
	
	/**
	 * @inheritDoc
	 */
	@Override
	public CategoryDTO findByName(String name) {

		if (name == null) {
			LOGGER.log(Level.WARNING, "NULL Parameter!");
			return null;
		}   

		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Category> criteriaQuery = criteriaBuilder
				.createQuery(Category.class);
		Root<Category> category = criteriaQuery.from(Category.class);
		criteriaQuery.where(criteriaBuilder.equal(category.get(Category_.name),
				name));
		criteriaQuery.select(category);
		TypedQuery<Category> q = em.createQuery(criteriaQuery);

		Category temp = new Category();
		try {
			temp = q.getSingleResult();
			CategoryDTO categoryOK = new CategoryDTO();
			if (temp != null) {

				categoryOK.setName(temp.getName());

				LOGGER.log(Level.INFO, "SUCCES !");
				return categoryOK;

			} else {

				LOGGER.log(Level.WARNING, "NULL Parameter!");
				return null;
			}

		} catch (Exception e) {

			LOGGER.log(Level.SEVERE, "FAIL Category !");
			e.printStackTrace();
			
		}
		return null;

	}
	
	@Override
	public int findIdByName(String name) {

		if (name == null) {
			LOGGER.log(Level.WARNING, "NULL Parameter!");
			return 0;
		}   

		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Category> criteriaQuery = criteriaBuilder
				.createQuery(Category.class);
		Root<Category> category = criteriaQuery.from(Category.class);
		criteriaQuery.where(criteriaBuilder.equal(category.get(Category_.name),
				name));
		criteriaQuery.select(category);
		TypedQuery<Category> q = em.createQuery(criteriaQuery);

		Category temp = new Category();
		try {
			temp = q.getSingleResult();
			CategoryDTO categoryOK = new CategoryDTO();
			if (temp != null) {

				categoryOK.setName(temp.getName());

				LOGGER.log(Level.INFO, "SUCCES !");
				return temp.getId();

			} else {

				LOGGER.log(Level.WARNING, "NULL Parameter!");
				return 0;
			}

		} catch (Exception e) {

			LOGGER.log(Level.SEVERE, "FAIL Category !");
			e.printStackTrace();
			
		}
		return 0;
	}

}
