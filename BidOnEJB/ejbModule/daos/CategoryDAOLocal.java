package daos;

import javax.ejb.Local;

import dto.CategoryDTO;

@Local
public interface CategoryDAOLocal {
	
	public boolean create(CategoryDTO categoryDTO);
	
	public CategoryDTO findByName(String name);

	public int findIdByName(String name);
}
