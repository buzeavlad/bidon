package daos;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import model.AuctionParticipant;
import model.Category;
import model.Category_;
import model.Product_;
import model.Review;
import model.AuctionParticipant.AuctionParticipantStatus;
import model.Product;
import dto.AuctionDTO;
import dto.AuctionParticipantDTO;
import dto.ProductDTO;
import dto.ReviewDTO;

/**
 * Session Bean implementation class ProductDAO
 */
@Stateless
public class ProductDAO implements ProductDAORemote,ProductDAOLocal {

	@PersistenceContext
	EntityManager em;
	
	@EJB
	CategoryDAOLocal categoryDAO;

	public void setEm(EntityManager em) {
		this.em = em;
	}

	private Logger LOGGER = Logger.getLogger(ProductDAO.class.getName());
	
	/**
	 * @inheritDoc
	 */
	@Override
	public int createProduct(ProductDTO productDTO) {
		if (productDTO == null) {
			LOGGER.log(Level.WARNING, "NULL productDTO !");
			return 0;
		}

		try {
			Product product = new Product();
			product.setName(productDTO.getName());
			product.setDescription(productDTO.getDescription());
			int idCategory = categoryDAO.findIdByName(productDTO.getCategory());
			Category category = em.find(Category.class,
					idCategory);
			product.setCategory(category);

			em.persist(product);
			em.flush();
			LOGGER.log(Level.INFO, "SUCCES !");
			return product.getId();

		} catch (Exception e) {

			LOGGER.log(Level.SEVERE, "FAIL Product !");
			e.printStackTrace();
		}
		return 0;
	}
	
	/**
	 * @inheritDoc
	 */
	@Override
	public boolean create(ProductDTO productDTO) {
		if (productDTO == null) {
			LOGGER.log(Level.WARNING, "NULL productDTO !");
			return false;
		}

		try {
			Product product = new Product();
			product.setName(productDTO.getName());
			product.setDescription(productDTO.getDescription());
			Category category = em.find(Category.class,
					productDTO.getCategory());
			product.setCategory(category);

			em.persist(product);

			LOGGER.log(Level.INFO, "SUCCES !");
			return true;

		} catch (Exception e) {

			LOGGER.log(Level.SEVERE, "FAIL Product !");
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * @inheritDoc
	 */
	@Override
	public boolean update(int id, ProductDTO productDTO) {
		Product lProduct = em.find(Product.class, id);

		if (lProduct == null) {
			LOGGER.log(Level.WARNING, "NULL productDTO !");
			return false;
		}

		else {

			lProduct.setName(productDTO.getName());
			lProduct.setDescription(productDTO.getDescription());

			Category category = em.find(Category.class,
					productDTO.getCategory());

			lProduct.setCategory(category);

			LOGGER.log(Level.INFO, "SUCCES !");
			return true;
		}

	}
	
	/**
	 * @inheritDoc
	 */
	@Override
	public List<ProductDTO> findAll() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Product> cq = cb.createQuery(Product.class);
		Root<Product> root = cq.from(Product.class);
		cq.select(root);

		TypedQuery<Product> q = em.createQuery(cq);
		List<Product> list = q.getResultList();
		
		return entityToDTO(list);
	}
	

	private List<ProductDTO> entityToDTO(List<Product> list){
		List<ProductDTO> list2 = new ArrayList<>();
		for (Product s : list) {

			ProductDTO s2 = new ProductDTO();
			
			s2.setName(s.getName());
			s2.setDescription(s.getDescription());
			s2.setCategory(s.getCategory().getName());
			list2.add(s2);
		}
		return list2;
	}

	
	/**
	 * ({@inheritDoc}
	 */
	@Override
	public ProductDTO findByID(int idProduct) {
		
		ProductDTO product = null;
		try {
			product = em.find(Product.class, idProduct).toDTO();
		} catch (Exception e) {
			LOGGER.log(Level.WARNING,"Product not found.");
			e.printStackTrace();
		}
			
		return product;
	}

}
