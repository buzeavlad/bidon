package daos;

import javax.ejb.Local;

import dto.ProductDTO;

@Local
public interface ProductDAOLocal {

	public ProductDTO findByID(int idProduct); 

	public int createProduct(ProductDTO productDTO);
	
}
