package daos;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.eclipse.persistence.internal.jpa.QueryImpl;

import model.Auction;
import model.AuctionParticipant;
import model.AuctionParticipant_;
import model.Auction_;
import model.Bid_;
import model.Review;
import model.Review_;
import model.User;
import dto.AuctionParticipantDTO;
import dto.ReviewDTO;
/**
 *  Class that implements the basic operations for Review
 * @author Cipri
 *
 */
@Stateless
public class ReviewDAO implements ReviewDAORemote {
	@PersistenceContext
	EntityManager em;

	public void setEm(EntityManager em) {
		this.em = em;
	}

	private Logger LOGGER = Logger.getLogger(ReviewDAO.class.getName());

	/**
	 * @inheritDoc
	 */
	@Override
	public boolean create(ReviewDTO reviewDTO) {

		if (reviewDTO == null) {
			LOGGER.log(Level.WARNING, "Parameter is null");
			return false;
		}

		try {
			Review review = new Review();
			review.setDescription(reviewDTO.getDescription());
			review.setRating(reviewDTO.getRating());

			Auction auction = em.find(Auction.class, reviewDTO.getIdAuction());
			review.setAuction(auction);

			em.persist(auction);

			LOGGER.log(Level.INFO, "Succes creating rewiew !");
			return true;

		} catch (Exception e) {

			LOGGER.log(Level.SEVERE, "FAIL creating review !");
			e.printStackTrace();
		}
		return false;

	}

	/**
	 * @inheritDoc
	 */
	@Override
	public List<ReviewDTO> findALL() {

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Review> cq = cb.createQuery(Review.class);
		Root<Review> root = cq.from(Review.class);
		cq.select(root);

		TypedQuery<Review> q = em.createQuery(cq);
		List<Review> list = q.getResultList();
		List<ReviewDTO> list2 = new ArrayList<>();
		for (Review s : list) {

			ReviewDTO s2 = new ReviewDTO();
			s2.setDescription(s.getDescription());
			s2.setIdAuction(s.getAuction().getId());
			s2.setRating(s.getRating());
			list2.add(s2);
		}
		return list2;
	}

	/**
	 * @inheritDoc
	 */
	@Override
	public boolean update(int id, ReviewDTO reviewDTO) {

		Review lReview = em.find(Review.class, id);

		if (lReview == null) {
			LOGGER.log(Level.WARNING, "reviewDTO is null !");
			return false;
		}

		else {

			lReview.setDescription(reviewDTO.getDescription());
			lReview.setRating(reviewDTO.getRating());

			Auction auction = em.find(Auction.class, reviewDTO.getIdAuction());
			lReview.setAuction(auction);

			LOGGER.log(Level.INFO, "Review updated succesfully !");
			return true;
		}
	}

	/**
	 * @inheritDoc
	 */
	@Override
	public List<ReviewDTO> findReviewByUser(int idSeller) {
		if (idSeller == 0) {
			LOGGER.log(Level.INFO, "IDSeller null");
			return null;
		}

		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Review> criteriaQuery = criteriaBuilder
				.createQuery(Review.class);

		Root<Review> root = criteriaQuery.from(Review.class);
		Join<Review, Auction> auction = root.join(Review_.auction);
		Join<Auction, AuctionParticipant> auctionParticipant = auction.join(Auction_.auctionparticipant);
		
		Predicate condition = criteriaBuilder.equal(auctionParticipant.get(AuctionParticipant_.id), idSeller);
		
		criteriaQuery.where(condition);

		TypedQuery<Review> q = em.createQuery(criteriaQuery);
		List<Review> list = q.getResultList();
		List<ReviewDTO> list2 = new ArrayList<>();
		for (Review s : list) {

			ReviewDTO s2 = new ReviewDTO();
			s2.setDescription(s.getDescription());
			s2.setIdAuction(s.getAuction().getId());
			s2.setRating(s.getRating());
			list2.add(s2);
		}
		return list2;
	}
}
