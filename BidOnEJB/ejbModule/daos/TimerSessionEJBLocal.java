/**
 * 
 */
package daos;

import java.io.Serializable;
import java.util.Date;

import javax.ejb.Local;
import javax.ejb.Timer;

import model.Auction;

/**
 * @author Cipri
 * a local Interface that describes the behavior of an EJB (TimerSessionEJB)
 */
@Local 
public interface TimerSessionEJBLocal {

	/**
	 * Method that is called to start the timer 
	 * 
	 * @param dueDate- the date when the timer will stop
	 * @param auction- the action associated to the timer
	 */
	public void startTimer(Date dueDate, Auction auction);
	
	public void killTimer(Integer value);
}
