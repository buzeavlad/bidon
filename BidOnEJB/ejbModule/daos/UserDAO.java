package daos;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import utils.EmailManager;
import utils.EmailMessage;
import model.User;
import model.User_;
import dto.UserDTO;


/**
 * @author Zanc Razvan
 * Session Bean implementation class UserDAO
 */
@Stateless
public class UserDAO implements UserDAORemote {
	@PersistenceContext
	EntityManager em;

	/**
	 * Default constructor.
	 */

	private EmailManager emailManager;

	private EmailMessage message;
	
	public UserDAO() {
		
	}

	/**
	 *  Insert an userDTO into database 
	 *  
	 *  @param userDTO
	 *  @return true -> if the user was successfully inserted
	 *  @return false -> if the user wasn't inserted
	 */
	@Override
	public boolean create(UserDTO userDTO) {

		if (userDTO == null) {
			System.out.println("UserDTO dat ca parametru e null");
			return false;
		}   

		try {
			User user = new User();
			user.setEmail(userDTO.getEmail());
			user.setFirstName(userDTO.getFirstName());
			user.setLastName(userDTO.getLastName());
			user.setPassword(userDTO.getPassword());
			user.setUsername(userDTO.getUsername());
			em.persist(user);

			System.out.println("Userul a fost creat cu succes !");
			return true;

		} catch (Exception e) {

			System.out.println("Userul nu a fost creat !");
			e.printStackTrace();
		}
		return false;
	}

	/**
	 *  Update an user from the database
	 *  
	 *  @param userDTO
	 *  @param id
	 *  @return true -> if the user was successfully updated
	 *  @return false -> if the user wasn't updated
	 */
	@Override
	public boolean update(int id, UserDTO userDTO) {
		User lUser = em.find(User.class, id);

		if (lUser == null) {
			System.out.println("User'ul nu a fost gasit !");
			return false;
		}

		else {

			lUser.setEmail(userDTO.getEmail());
			lUser.setFirstName(userDTO.getFirstName());
			lUser.setLastName(userDTO.getLastName());
			lUser.setPassword(userDTO.getPassword());
			lUser.setUsername(userDTO.getUsername());

			System.out.println("User'ul a fost updatat cu succes !");
			return true;
		}

	}

	

	/**
	 *  Function which updates user password and sends an email to the user with the new password 
	 *  
	 *  @param userDTO
	 *  @param id
	 *  @return true -> if the user was successfully updated 
	 *  @return false -> if the user wasn't updated
	 */
	@Override
	public boolean updateAndSendMail(int id, UserDTO userDTO) {
		User lUser = em.find(User.class, id);

		if (lUser == null) {
			System.out.println("User'ul nu a fost gasit !");
			return false; 
		}
     
		else {
			lUser.setPassword(userDTO.getPassword());
			
			System.out.println("User'ul a fost updatat cu succes !");
		    emailManager = new EmailManager();
			message = new EmailMessage();

			emailManager.sendMail("bid090814@gmail.com", userDTO.getEmail(), "subiect", message.createForgotPasswordMessage(userDTO));
		
			
			return true;
		}

	}
	/**
	 *  Delete an user from the database
	 *  
	 *  @param username
	 *  @return true -> if the user was successfully deleted
	 *  @return false -> if the user wasn't deleted
	 */
	@Override
	public boolean delete(String username) {

		User lUser = em.find(User.class, username);

		if (lUser == null) {
			System.out.println("Userul cu username'ul " + username
					+ " nu a fost gasit !");

			return false;
		}

		else {

			try {
				User user1 = em.merge(lUser);
				em.remove(user1);

				System.out.println("Userul cu username'ul " + username
						+ " a fost sters cu succes !");
				return true;

			} catch (Exception e) {

				System.out.println("Userul cu username'ul " + username
						+ " nu a fost sters cu succes !");
				e.printStackTrace();
				return false;
			}
		}

	}

	/**
	 *  Find an user using it's username
	 *  
	 *  @param username
	 *  @return UserDTO -> if the user was successfully found
	 *  @return null -> if the user wasn't found
	 */
	@Override
	public UserDTO findByUsername(String username) {

		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<User> criteriaQuery = criteriaBuilder
				.createQuery(User.class);
		Root<User> user = criteriaQuery.from(User.class);
		criteriaQuery.where(criteriaBuilder.equal(user.get(User_.username),
				username));
		criteriaQuery.select(user);
		TypedQuery<User> q = em.createQuery(criteriaQuery);

		User temp = new User();
		try {
			temp = q.getSingleResult();
			UserDTO userOK = new UserDTO();
			if (temp != null) {

				userOK.setPassword(temp.getPassword());
				userOK.setUsername(temp.getUsername());
				userOK.setEmail(temp.getEmail());
				userOK.setFirstName(temp.getFirstName());
				userOK.setLastName(temp.getLastName());

				System.out.println("Userul a fost gasit cu succes !");
				return userOK;

			} else {

				System.out.println("Userul nu a fost gasit");
				return null;
			}

		} catch (Exception e) {

			System.out.println("Userul nu a fost gasit");
			e.printStackTrace();
			return null;
		}

	}
	
	/**
	 *  Find an user using it's email
	 *  
	 *  @param username
	 *  @return UserDTO -> if the user was successfully found
	 *  @return null -> if the user wasn't found
	 */
	@Override
	public UserDTO findByEmail(String email) {

		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<User> criteriaQuery = criteriaBuilder
				.createQuery(User.class);
		Root<User> user = criteriaQuery.from(User.class);
		criteriaQuery
				.where(criteriaBuilder.equal(user.get(User_.email), email));
		criteriaQuery.select(user);
		TypedQuery<User> q = em.createQuery(criteriaQuery);

		User temp = new User();
		try {
			temp = q.getSingleResult();
			UserDTO userOK = new UserDTO();
			if (temp != null) {

				userOK.setPassword(temp.getPassword());
				userOK.setUsername(temp.getUsername());
				userOK.setEmail(temp.getEmail());
				userOK.setFirstName(temp.getFirstName());
				userOK.setLastName(temp.getLastName());

				System.out.println("Userul a fost gasit cu succes !");
				return userOK;

			} else {

				System.out.println("Userul nu a fost gasit");
				return null;
			}

		} catch (Exception e) {

			System.out.println("Userul nu a fost gasit");
			e.printStackTrace();
			return null;
		}

	}

	/**
	 *  Find an user using it's email
	 *  
	 *  @param username
	 *  @return UserDTO -> if the user was successfully found
	 *  @return null -> if the user wasn't found
	 */
	@Override
	public int findIdByUsername(String username) {
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<User> criteriaQuery = criteriaBuilder
				.createQuery(User.class);
		Root<User> user = criteriaQuery.from(User.class);
		criteriaQuery
				.where(criteriaBuilder.equal(user.get(User_.username), username));
		criteriaQuery.select(user);
		TypedQuery<User> q = em.createQuery(criteriaQuery);

		User temp = new User();
		try {
			temp = q.getSingleResult();
			if (temp != null) {

				System.out.println("Id'ul a fost gasit ! Este: "+temp.getId());
				return temp.getId();

			} else {

				System.out.println("Username'ul nu a fost gasit !");
				return -1;
			}

		} catch (Exception e) {

			System.out.println("Id'ul nu a fost gasit");
			e.printStackTrace();
			return -1;
		}
	}
	


	/**
	 * @param em the em to set
	 */
	public void setEm(EntityManager em) {
		this.em = em;
	}

}
