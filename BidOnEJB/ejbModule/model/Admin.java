package model;

import javax.persistence.*;


/**
 * 
 * @author Vlad Buzea
 * The persistent class for the Administration database table.
 *
 */ 
@Entity
@NamedQuery(name="Admin.findAll", query="SELECT a FROM Admin a")
@DiscriminatorValue(value="Admin")
public class Admin extends User {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(nullable=false)
	private int authorityLevel;


	public Admin() {
	}


	public int getAutorityLevel() {
		return this.authorityLevel;
	}

	public void setAutorityLevel(int autorityLevel) {
		this.authorityLevel = autorityLevel;
	}

}
