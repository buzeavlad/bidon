package model;

import java.io.Serializable;

import javax.persistence.*;

import dto.AuctionDTO;
import dto.AuctionListDTO;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;


/**
 * The persistent class for the auction database table.
 * 
 */
@Entity
@NamedQuery(name="Auction.findAll", query="SELECT a FROM Auction a")
public class Auction implements Serializable {
	public static enum AuctionStatus {RUNNING,FINISHED,PENDING }
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date dueDate;

	@Column(nullable=false)
	private int incrementRate;

	@Column(nullable=false)
	private int startingPrice;

	@Column(nullable=false)
	private AuctionStatus status;

	//bi-directional many-to-one association to Product
	@ManyToOne
	@JoinColumn(name="produs",nullable=false)
	private Product product;

	//bi-directional many-to-one association to Auctionparticipant
	@ManyToOne
	@JoinColumn(name="idSeller",nullable=false)
	private AuctionParticipant auctionparticipant;
	
	@OneToMany(mappedBy="auction",fetch=FetchType.EAGER)
	private Set<Bid> bids = new TreeSet<>();

	/**
	 * @return the bids
	 */
	public Set<Bid> getBids() {
		return bids;
	}

	/**
	 * @param bids the bids to set
	 */
	public void setBids(Set<Bid> bids) {
		this.bids = bids;
	}

	public Auction() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	 
	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public int getIncrementRate() {
		return this.incrementRate;
	}

	public void setIncrementRate(int incrementRate) {
		this.incrementRate = incrementRate;
	}

	public int getStartingPrice() {
		return this.startingPrice;
	}

	public void setStartingPrice(int startingPrice) {
		this.startingPrice = startingPrice;
	}

	
	public AuctionStatus getStatus() {
		return status;
	}

	public void setStatus(AuctionStatus status) {
		this.status = status;
	}

	public Product getProduct() {
		return this.product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public AuctionParticipant getAuctionparticipant() {
		return this.auctionparticipant;
	}

	public void setAuctionparticipant(AuctionParticipant auctionparticipant) {
		this.auctionparticipant = auctionparticipant;
	}
	
	
	public void addBid(Bid bid){
		if(bid.getAuction().getId()==this.getId())
			bids.add(bid);
	}
	
	public void removeBid(Bid bid){
		bids.remove(bid);
	}

	public AuctionDTO toDTO(){
		AuctionDTO auctionDTO = new AuctionDTO(id, getAuctionparticipant().getId(), startingPrice, incrementRate, dueDate, getProduct().getId(), "");
		if (status == AuctionStatus.RUNNING){
			auctionDTO.setStatus(AuctionDTO.RUNNING);
		} else if (status == AuctionStatus.FINISHED){
			auctionDTO.setStatus(AuctionDTO.FINISHED);
		} else {
			auctionDTO.setStatus(AuctionDTO.PENDING);
		}
		return auctionDTO;
	}

	public AuctionListDTO toListDTO(){
		AuctionListDTO auctionListDTO = new AuctionListDTO(product.getName(), dueDate, 0, startingPrice, auctionparticipant.getUsername(),this.getId());
		auctionListDTO.setStatus(this.status.toString());
		return auctionListDTO;
	}
	
	public Bid getMaxBid(){
		if(bids!=null){
			TreeSet<Bid> tree=new TreeSet<Bid>(bids);
			if(tree.size()>0)
				return tree.last();
		}
		return null;
	}

	
	
}
