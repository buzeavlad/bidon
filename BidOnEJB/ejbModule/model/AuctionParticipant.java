package model;


import java.util.List;

import javax.persistence.*;

import dto.AuctionParticipantDTO;



/**
 * The persistent class for the auctionparticipant database table.
 * 
 */
@Entity
@NamedQuery(name="AuctionParticipant.findAll", query="SELECT a FROM AuctionParticipant a")
@DiscriminatorValue(value="AuctionParticipant")
public class AuctionParticipant extends User {
	public static enum AuctionParticipantStatus{PENDING,VALID,INVALID,BANNED,BLOCKED}
	
	private static final long serialVersionUID = 1L;

	@Column(nullable=false)
	private String address;

	@Column(nullable=false)
	private int bidcoins;

	private String phone;

	@Column(nullable=false)
	private AuctionParticipantStatus status;
	
	@Column(columnDefinition="BOOLEAN default true NOT NULL")
	private boolean subscribedForEmail;
	

	
	@OneToMany(mappedBy="auctionparticipant",fetch=FetchType.EAGER)
	private List<Auction> createdAuctions;


	public AuctionParticipant() {
	}


	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getBidcoins() {
		return this.bidcoins;
	}

	public void setBidcoins(int bidcoins) {
		this.bidcoins = bidcoins;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}


	public AuctionParticipantStatus getStatus() {
		return status;
	}


	public void setStatus(AuctionParticipantStatus status) {
		this.status = status;
	}

	
	/**
	 * @return the createdAuctions
	 */
	public List<Auction> getCreatedAuctions() {
		return createdAuctions;
	}

	/**
	 * @param createdAuctions the createdAuctions to set
	 */
	public void setCreatedAuctions(List<Auction> createdAuctions) {
		this.createdAuctions = createdAuctions;
	}

	public boolean isSubscribedForEmail() {
		return subscribedForEmail;
	}

	public void setSubscribedForEmail(boolean subscribedForEmail) {
		this.subscribedForEmail = subscribedForEmail;
	}
	
	public AuctionParticipantDTO toDTO(){
		AuctionParticipantDTO ap = new AuctionParticipantDTO(getEmail(), getFirstName(), getLastName(), getPassword(), getUsername(), getAddress(), getBidcoins(), getPhone(), isSubscribedForEmail(),getStatus().toString());
		return ap;
	}	

}