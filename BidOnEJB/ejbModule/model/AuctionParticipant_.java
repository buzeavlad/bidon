package model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import model.AuctionParticipant.AuctionParticipantStatus;

@Generated(value="Dali", date="2014-08-28T16:07:48.030+0300")
@StaticMetamodel(AuctionParticipant.class)
public class AuctionParticipant_ extends User_ {
	public static volatile SingularAttribute<AuctionParticipant, String> address;
	public static volatile SingularAttribute<AuctionParticipant, Integer> bidcoins;
	public static volatile SingularAttribute<AuctionParticipant, String> phone;
	public static volatile SingularAttribute<AuctionParticipant, AuctionParticipantStatus> status;
	public static volatile SingularAttribute<AuctionParticipant, Boolean> subscribedForEmail;
	public static volatile ListAttribute<AuctionParticipant, Auction> createdAuctions;
}
