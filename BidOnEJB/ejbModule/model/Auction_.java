package model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import model.Auction.AuctionStatus;

@Generated(value="Dali", date="2014-08-28T16:07:48.021+0300")
@StaticMetamodel(Auction.class)
public class Auction_ {
	public static volatile SingularAttribute<Auction, Integer> id;
	public static volatile SingularAttribute<Auction, Date> dueDate;
	public static volatile SingularAttribute<Auction, Integer> incrementRate;
	public static volatile SingularAttribute<Auction, Integer> startingPrice;
	public static volatile SingularAttribute<Auction, AuctionStatus> status;
	public static volatile SingularAttribute<Auction, Product> product;
	public static volatile SingularAttribute<Auction, AuctionParticipant> auctionparticipant;
	public static volatile SetAttribute<Auction, Bid> bids;
}
