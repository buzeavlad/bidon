package model;

import java.io.Serializable;

import javax.persistence.*;

import dto.BidDTO;

import java.util.Date;


/**
 * The persistent class for the bid database table.
 * 
 */
@Entity
@NamedQuery(name="Bid.findAll", query="SELECT b FROM Bid b")
public class Bid implements Serializable,Comparable<Bid> {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(nullable=false)
	private int amount;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable=false)
	private Date date;

	//bi-directional many-to-one association to Auction
	@ManyToOne
	@JoinColumn(name="idAuction",nullable=false)
	private Auction auction;

	//bi-directional many-to-one association to Auctionparticipant
	@ManyToOne
	@JoinColumn(name="idBidder",nullable=false)
	private AuctionParticipant auctionparticipant;

	public Bid() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}
 
	public int getAmount() {
		return this.amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	
	

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Auction getAuction() {
		return this.auction;
	}

	public void setAuction(Auction auction) {
		this.auction = auction;
	}

	public AuctionParticipant getAuctionparticipant() {
		return this.auctionparticipant;
	}

	public void setAuctionparticipant(AuctionParticipant auctionparticipant) {
		this.auctionparticipant = auctionparticipant;
	}


	@Override
	public int compareTo(Bid o) {
		Bid temp;
		if(o instanceof Bid){
			temp=(Bid)o;
			if(this.getAmount()>temp.getAmount())
				return 1;
			if(this.getAmount()<temp.getAmount())
				return -1;
			}
		return 0;
	}

	public BidDTO toDTO(){
		return new BidDTO(amount, date, auction.getId(), auctionparticipant.getId());
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Bid [id=" + id + ", amount=" + amount + ", date=" + date
				+ ", auction=" + auction + ", auctionparticipant="
				+ auctionparticipant + "]";
	}
	
	
}