package model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2014-08-18T10:02:22.126+0300")
@StaticMetamodel(Bid.class)
public class Bid_ {
	public static volatile SingularAttribute<Bid, Integer> id;
	public static volatile SingularAttribute<Bid, Integer> amount;
	public static volatile SingularAttribute<Bid, Date> date;
	public static volatile SingularAttribute<Bid, Auction> auction;
	public static volatile SingularAttribute<Bid, AuctionParticipant> auctionparticipant;
}
