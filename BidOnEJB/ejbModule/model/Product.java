package model;

import java.io.Serializable;

import javax.persistence.*;

import dto.ProductDTO;


/**
 * The persistent class for the product database table.
 * 
 */
@Entity
@NamedQuery(name="Product.findAll", query="SELECT p FROM Product p")
public class Product implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(nullable=false)
	private String description;

	private byte[] image;

	@Column(nullable=false)
	private String name;

	//bi-directional many-to-one association to Category
	@ManyToOne
	@JoinColumn(name="category",nullable=false)
	private Category category;

	public Product() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public byte[] getImage() {
		return this.image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Category getCategory() {
		return this.category;
	}

	public void setCategory(Category categoryBean) {
		this.category = categoryBean;
	}

	public ProductDTO toDTO(){
		return new ProductDTO(description, name, category.getName());
	}
	
}