package model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2014-08-28T16:07:48.037+0300")
@StaticMetamodel(Product.class)
public class Product_ {
	public static volatile SingularAttribute<Product, Integer> id;
	public static volatile SingularAttribute<Product, String> description;
	public static volatile SingularAttribute<Product, byte[]> image;
	public static volatile SingularAttribute<Product, String> name;
	public static volatile SingularAttribute<Product, Category> category;
}
