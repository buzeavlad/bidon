package model;

import java.io.Serializable;

import javax.persistence.*;

import dto.ReviewDTO;


/**
 * The persistent class for the review database table.
 * 
 */
@Entity
@NamedQuery(name="Review.findAll", query="SELECT r FROM Review r")
public class Review implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(nullable=false)
	private String description;

	@Column(nullable=false)
	private int rating;

	//bi-directional many-to-one association to Auction
	@ManyToOne
	@JoinColumn(name="auction",nullable=false)
	private Auction auction;

	public Review() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getRating() {
		return this.rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public Auction getAuction() {
		return this.auction;
	}

	public void setAuction(Auction auction) {
		this.auction = auction;
	}

	public ReviewDTO toDTO(){
		return new ReviewDTO(description, rating, auction.getId());
	}
}