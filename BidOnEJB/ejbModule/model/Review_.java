package model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2014-08-18T10:02:22.137+0300")
@StaticMetamodel(Review.class)
public class Review_ {
	public static volatile SingularAttribute<Review, Integer> id;
	public static volatile SingularAttribute<Review, String> description;
	public static volatile SingularAttribute<Review, Integer> rating;
	public static volatile SingularAttribute<Review, Auction> auction;
}
