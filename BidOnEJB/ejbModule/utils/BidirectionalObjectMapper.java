package utils;
/*
 * Schnittstelle für einen bidirektionalen Object-Mapper.
 *
 */
public interface BidirectionalObjectMapper<K, O>
{
    K getKey(O aObject);

    O getObject(K aKey);
}
