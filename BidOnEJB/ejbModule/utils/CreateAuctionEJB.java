package utils;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import model.Auction;
import model.Bid;
import daos.AuctionDAO;
import daos.AuctionDAOLocal;
import daos.BidDAOLocal;
import daos.CategoryDAOLocal;
import daos.ProductDAOLocal;
import daos.TimerSessionEJBLocal;
import dto.AuctionDTO;
import dto.BidDTO;
import dto.CategoryDTO;
import dto.ProductDTO;

/**
 * @author marius.sutea
 * This class starts an auction, which is sent from BidOnWebs
 * Session Bean implementation class CreateAuctionEJB
 */
@Stateless
@LocalBean
public class CreateAuctionEJB implements CreateAuctionEJBRemote {

	@EJB
	TimerSessionEJBLocal timer;
	@EJB
	AuctionDAOLocal auctionDAO;
	@EJB
	ProductDAOLocal productDAO;
	@EJB
	BidDAOLocal bidDAO;
	@EJB
	CategoryDAOLocal categoryDAO;
   
	private Logger LOGGER;
    
	/**
	 * Calls the method create from AuctionDAO and starts the timer
	 * @param date The date in which the auction ends
	 * @param auctionDTO The auctionDTO used to insert a new auction in the database
	 */
	@Override
	public void startAuction(Date date, AuctionDTO auctionDTO, ProductDTO productDTO) {
		if (auctionDTO==null) {
			LOGGER.log(Level.WARNING, "The auctionDTO given as parameter is null");
		}
		
		if (categoryDAO.findByName(productDTO.getCategory())==null) {
			categoryDAO.create(new CategoryDTO(productDTO.getCategory()));
		}
		
		int productId = productDAO.createProduct(productDTO);
		auctionDTO.setIdProduct(productId);
		Auction auction = auctionDAO.create(auctionDTO);
		
	//	Bid bid = bidDAO.createBid(new BidDTO(135,new Date(java.lang.System.currentTimeMillis()+30000),auction.getId(),4));
		
	//	auction.addBid(bid);
		timer.startTimer(date,auction);	
		
	}
}