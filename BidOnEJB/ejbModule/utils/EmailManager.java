/**
 * 
 */
package utils;

/**
 * @author Cipri
 * class that manage an object which will send a mail from an address of g-mail to a G-mail address
 */

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Properties;
import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;

public class EmailManager {


	private class MailAuthenticator extends Authenticator {

		String username = "bid090814@gmail.com";

		String password = "adminadmin1234";

		public MailAuthenticator() {
			// TODO Auto-generated constructor stub
		}

		protected PasswordAuthentication getPasswordAuthentication() {
			return new PasswordAuthentication(username, password);
		}

	}

	public void sendMail(String addressFrom, String addressTo, String subject, String mailMessage) {

		String host = "smtp.gmail.com";
		String from = addressFrom;
		String to = addressTo;
		
		Address fromaddress = null, toaddress = null;

		Properties mailproperties = new Properties();

		mailproperties.put("mail.smtp.host", host);
		mailproperties.put("mail.smtp.port", "587");
		mailproperties.put("mail.debug", "true");
		mailproperties.put("mail.transport.protocal", "smtps");
		mailproperties.put("mail.smtp.auth", "true");
		mailproperties.put("mail.smtps.**ssl.enable", "false");
		mailproperties.put("mail.smtp.starttls.enable", "true");
		mailproperties.put("mail.smtp.EnableSSL.enable", "true");
		mailproperties.put("mail.smtps.**ssl.required", "false");

		EmailManager emailgenerator = new EmailManager();

		MailAuthenticator auth = emailgenerator.new MailAuthenticator();

		Session session = Session.getInstance(mailproperties, auth);

		try {
			fromaddress = new InternetAddress(from, "Admin BidOn");
			toaddress = new InternetAddress(to);
			

		} catch (AddressException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		} catch (UnsupportedEncodingException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}

		Message message = new MimeMessage(session);

		try {
			message.setFrom(fromaddress);
			message.setRecipient(RecipientType.TO, toaddress);
			message.setSubject(subject);
			message.setSentDate(new Date());
			message.setText(mailMessage);

			Transport transport;
			transport = session.getTransport("smtp");
			transport.connect();
			message.saveChanges();
			transport.send(message);
			System.out.println("Mail send successfully");

		} catch (MessagingException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}
	}
}