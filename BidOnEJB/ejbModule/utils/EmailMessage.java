/**
 * 
 */
package utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.ejb.EJB;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import model.Auction;
import model.AuctionParticipant;
import model.Bid;
import daos.AuctionDAOLocal;
import daos.AuctionDAORemote;
import daos.AuctionParticipantDAORemote;
import dto.AuctionDTO;
import dto.AuctionListDTO;
import dto.AuctionParticipantDTO;
import dto.UserDTO;

/**
 * @author Cipri
 *
 */
public class EmailMessage {

	@EJB
	private AuctionParticipantDAORemote auctionParticipantDAO;

	@EJB
	private AuctionDAOLocal auctionDAO;

	@PersistenceContext
	EntityManager em;

	/**
	 * Method that creates the message String which will be send to a seller when the auction is finished
	 * 
	 * @param auction- The auction that will finish when the message is send
	 * @return the message for Seller
	 */
	public String createSellerMessage(Auction auction, AuctionParticipantDTO winner) {

		String productName = auction.getProduct().getName();
		String sellerLastName = auction.getAuctionparticipant().getLastName();
		String sellerFirstName = auction.getAuctionparticipant().getFirstName();
		int price = auction.getMaxBid().getAmount();

//		AuctionParticipantDTO auctionParticipantDTO = auctionDAO
//				.findWinner(auction.getId());
		
//		AuctionParticipant auctionParticipant = em.find(
//				AuctionParticipant.class, auctionParticipantDAO
//						.findIdByUsername(winner.getUsername()));

		String winnerFirstName = null;
		String winnerLastName = null;

		if (winner != null) {

			winnerLastName = winner.getLastName();
			winnerFirstName = winner.getFirstName();
		}

		String message = "Hello, "
				+ sellerFirstName
				+ " "
				+ sellerLastName
				+ " ./nYour auction has finished. Your product has been sold with the price: "
				+ price + " /n/nThe winner is: " + winnerFirstName + " "
				+ winnerLastName + ".";

		return message;
	}
	
	/**
	 * Method that creates the message String which will be send to a winner when the auction is finished
	 * 
	 * @param auction The auction that will finish when the message is send
	 * @return the message for winner
	 */
	public String createWinnerMessage(Auction auction, AuctionParticipantDTO winner) {

		String productName = auction.getProduct().getName();
		String sellerLastName = auction.getAuctionparticipant().getLastName();
		String sellerFirstName = auction.getAuctionparticipant().getFirstName();
		int price = auction.getMaxBid().getAmount();

//		AuctionParticipantDTO auctionParticipantDTO = auctionDAO
//				.findWinner(auction.getId());

//		AuctionParticipant auctionParticipant = em.find(
//				AuctionParticipant.class, auctionParticipantDAO
//						.findIdByUsername(winner.getUsername()));

		String winnerFirstName = null;
		String winnerLastName = null;

		if (winner != null) {

			winnerLastName = winner.getLastName();
			winnerFirstName = winner.getFirstName();
		}

		String message = "Hello, "
				+ winnerFirstName
				+ " "
				+ winnerLastName
				+ " .\nYour auction has finished. You are the winner of the auction. You have bought "+productName+" with the price: "
				+ price + ".\n\n ";

		return message;
	}
	
	/**
	 * Method that creates the message String which will be send to all the bidders that bit to that auction, when the auction is finished
	 * 
	 * @param auction The auction that will finish when the message is send
	 * @param bid The bid which contains the bidder we want to send themessage
	 * @return the message for bidder
	 */
	public String createAllBiddersMessage(Auction auction, Bid bid, AuctionParticipantDTO winner) {

		String productName = auction.getProduct().getName();
		String sellerLastName = auction.getAuctionparticipant().getLastName();
		String sellerFirstName = auction.getAuctionparticipant().getFirstName();
		int price = auction.getMaxBid().getAmount();
		
		String bidderFirstName=bid.getAuctionparticipant().getFirstName();
		String bidderLastName=bid.getAuctionparticipant().getLastName();

//		AuctionParticipantDTO auctionParticipantDTO = auctionDAO
//				.findWinner(auction.getId());

//		AuctionParticipant auctionParticipant = em.find(
//				AuctionParticipant.class, auctionParticipantDAO
//						.findIdByUsername(auctionParticipantDTO.getUsername()));

		int id=auction.getId();
		
		String winnerFirstName = null;
		String winnerLastName = null;

		if (winner != null) {

			winnerLastName = winner.getLastName();
			winnerFirstName = winner.getFirstName();
		}

		String message = "Hello, "
				+ bidderFirstName
				+ " "
				+ bidderLastName
				+ " .\nThe auction with the id "+id+" has finished. The winner of the auction is "+winnerLastName+" "+winnerFirstName +". Product was "+productName+" with the price: "
				+ price + ".\n\n ";

		return message;
	}

	public String createNoBidsMessage(Auction auction) {
		  
		  String sellerLastName = auction.getAuctionparticipant().getLastName();
		  String sellerFirstName = auction.getAuctionparticipant().getFirstName();
		  String link = "http://localhost:8080/BidOnWeb/faces/extendAuction.xhtml?id="+ auction.getId();

		  String message = "Hello, "
		    + sellerFirstName
		    + " "
		    + sellerLastName
		    + " ./nYour auction has finished without bidders.To extend the time click the link below:   "
		    + link;

		  return message;
		 }
	
	/**
	 * Creates the message of the email which will be sent to a user once an admin approves his account.
	 * It contains a link which will redirect the user to the login page.
	 * @author Stefi Marcu
	 * @param auctionParticipant
	 * @return a message
	 */
	public String createLinkConfirmationMessage(
			AuctionParticipantDTO auctionParticipant) {

		String firstName = auctionParticipant.getFirstName();
		String lastName = auctionParticipant.getLastName();
		String username = auctionParticipant.getUsername();
		String link = "http://localhost:8080/BidOnWeb/faces/login.xhtml";

		String message = "Thank you, " + firstName + " for registration!" + "\n\n"
				+ "These are the details of your account: " + "\n\n" 
				+ "Name: " + firstName + " " + lastName + "\n" 
				+ "Username: " + username + "\n"
				+ "Click on the following link to activate your account: \n"
				+ link;

		return message;
	}
	
	/**
	 * Creates the message of the email which will be sent to a user who forgot his password.
	 * It contains the new password 
	 * It contains a link which will redirect the user to the login page.
	 * 
	 * @author Carina Paul
	 * @param auctionParticipant
	 * @return a message
	 */ 
	public String createForgotPasswordMessage(UserDTO auctionParticipant) {

		String username = auctionParticipant.getUsername();
		String newpass = auctionParticipant.getEmail();
		String link = "http://localhost:8080/BidOnWeb/faces/login.xhtml";    

		String message = "Hello, " + username + "!" + "\n\n"
				+ "This is your new password you've requested!" + newpass+ "\n" 
				+ "Click on the following link to login with your new password! \n"
				+ link;

		return message;
	}
	
	
	/**
	 * Creates the email message for outbidded users to be notified.
	 * Contains a link that redirects to the auction's page.
	 */
	public String createOutBidNotification(AuctionParticipantDTO auctionParticipant ,int auctionID ,String productName) {
		String firstName = auctionParticipant.getFirstName();
		String lastName = auctionParticipant.getLastName();
		String link = "http://localhost:8080/BidOnWeb/faces/auctionDetails.xhtml?id=" + auctionID;
		
		String message = "Thank you, " + firstName + " " + lastName + " for participating to " 
				+ productName + " auction.\n" + "You have been outbidded, you can enter auction again by clicking this link\n" 
				+ link + "\nThis is a notification, please do not respond to this email!\nBidOn Team";
		
		return message;
	}
	
} 
