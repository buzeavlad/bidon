package utils;

import java.util.Date;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import model.Auction;
import daos.AuctionDAOLocal;
import daos.TimerSessionEJBLocal;
import dto.AuctionDTO;

@Stateless
@LocalBean
public class ExtendAuctionEJB implements ExtendAuctionEJBRemote{
	@EJB
	TimerSessionEJBLocal timer;
	
	@EJB
	AuctionDAOLocal auctionDAO;
	@Override
	public void extendAuction(Date date, AuctionDTO auctionDTO) {
		auctionDTO.setDueDate(date);
		auctionDTO.setStatus(AuctionDTO.RUNNING);
		int id = auctionDTO.getId();
		auctionDAO.update(id, auctionDTO);
		Auction auction = auctionDAO.findAuctionById(id);
		timer.startTimer(date,auction);	
		
	}

}
