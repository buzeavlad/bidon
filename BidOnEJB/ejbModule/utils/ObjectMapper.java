package utils;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Mapper zum bidirektionalen "navigieren" von Objekt-1 zu Object-2 und umgekehrt.
 *
 */
public class ObjectMapper<K,O> implements BidirectionalObjectMapper<K,O>
{
    private Map<K,O> key2ObjectHashtable = new HashMap<K,O>();
    private Map<O,K> object2KeyHashtable = new HashMap<O,K>();

    /**
     * Fügt ein bidirektionales Mapping aKey <-> aObject hinzu.
     * Zuvor werden alle anderen Mappings aKey -> xxx bzw. aObject -> yyy entfernt.
     *
     * @param aKey
     * @param aObject
     */
    public void addMapping(K aKey, O aObject)
    {
        // Wichtig: vor Hinzufügen des Mappings alte Mappings ersetzen!!!
        // Bugfix -> für Speicherleck verantwortlich, da alte CuAtWrapper und damit alte PersistencyProviderImpl dadurch referenziert blieben:
        removeMapping(aKey, aObject);

        key2ObjectHashtable.put(aKey, aObject);
        object2KeyHashtable.put(aObject, aKey);
    }

    /**
     * Entfernt Mappings von und zum Key ebenso wie Mappings von und zum Objekt.
     *
     * @param aKey
     * @param aObject
     */
    public void removeMapping(K aKey, O aObject)
    {
        // Nachschauen ob der Key schon mal registriert war:
        O previousObject = key2ObjectHashtable.get(aKey);
        // Object das vorher für den Key hinterlegt war rausnehmen.
        // Szenario: vorher: key -> objectA
        // Nun: addMapping key -> objectB
        // Somit muss objectA -> key aus object2KeyHashtable raus, da es sonst bidirektionales Navigieren verletzt:
        // objectA -> key -> objectB
        if (previousObject != null)
        {
            object2KeyHashtable.remove(previousObject);
        }
        // Analog andersrum:
        K previousKey = object2KeyHashtable.get(aObject);
        // Szenario: vorher keyA -> object
        // Nun: addMapping keyB -> object
        // Somit muss keyA -> object aus key2ObjectHashtable raus, da es sonst bidirektionales Navigieren verletzt:
        // keyA -> object -> keyB
        if (previousKey != null)
        {
            key2ObjectHashtable.remove(previousKey);
        }

        // bidirektionale Mappings für das Objekt und den Key auch noch entfernen:
        object2KeyHashtable.remove(aObject);
        key2ObjectHashtable.remove(aKey);
    }

    /**
     * Zum Abfragen eines Mappings Object2 -> Object1
     *
     * @param aObject
     * @return Object1
     */
    public K getKey(O aObject)
    {
        if (aObject == null)
        {
            return null;
        }

        return object2KeyHashtable.get(aObject);
    }

    /**
     * Zum Abfragen eines Mappings Object1 -> Object2
     *
     * @param aKey
     * @return Object2
     */
    public O getObject(K aKey)
    {
        if (aKey == null)
        {
            return null;
        }

        return key2ObjectHashtable.get(aKey);
    }

    /**
     * Liefert alle registrierten Schlüssel.
     *
     * @return alle registrierten Schlüssel.
     */
    public Set<K> getKeys()
    {
        return key2ObjectHashtable.keySet();
    }

}
