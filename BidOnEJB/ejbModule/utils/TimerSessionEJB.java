package utils;

import javax.ejb.Stateless;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;

import model.Auction;
import model.Auction.AuctionStatus;
import model.Bid;
import daos.AuctionDAOLocal;
import daos.AuctionDAORemote;
import daos.AuctionParticipantDAORemote;
import daos.BidDAOLocal;
import daos.BidDAORemote;
import daos.TimerSessionEJBLocal;
import dto.AuctionDTO;
import dto.AuctionParticipantDTO;

/**
 * The class that creates a timer for each auction. The timer expires when the
 * auction is finished
 * 
 * @author Cipri
 *
 */
@Stateless
@Singleton
public class TimerSessionEJB implements TimerSessionEJBRemote,
		TimerSessionEJBLocal {

	@Resource
	TimerService timerService;

	@EJB
	AuctionDAORemote auctionDAO;

	@EJB
	BidDAORemote bidDAORemote;

	@EJB
	BidDAOLocal bidDAOLocal;

	@EJB
	AuctionDAOLocal auctionDAOLocal;

	@EJB
	private AuctionParticipantDAORemote auctionParticipantDAO;

	private Auction auction;

	private Logger LOGGER = Logger.getLogger(TimerSessionEJB.class.getName());

	/**
	 * @inheritDoc
	 */
	public void startTimer(Date dueDate, Auction auction) {

		System.out.println("Timer Pornit");
		this.auction = auction;

		Timer timer = timerService.createSingleActionTimer(dueDate,
				new TimerConfig());

		TimerUtils.addAuction(timer, auction);
	}

	/**
	 * @inheritDoc
	 */
	@Timeout
	public void updateDB(Timer timer) {
		LOGGER.info("Timer has stopped.");

		Auction lAuction = auctionDAOLocal.findAuctionById(TimerUtils
				.getAuction(timer));
		System.out.println(lAuction.toString());

		int id = lAuction.getId();
		auctionDAO.finishAuction(id);
		// get winner id, get seller id

		EmailManager emailWinner = new EmailManager();
		EmailManager emailSeller = new EmailManager();
		EmailManager emailBidders = new EmailManager();

		String emailAddressWinner = new String(); // get winner's email;
		String emailAddressSeller = new String();// get seller's email

		EmailMessage message = new EmailMessage();
		

		/*
		 * if (bidDAORemote.findAllBidsFromAnAuction(auctionDAO
		 * .getIdByAuctionDTO(auction)) != null) {
		 */
		if (lAuction.getBids().size() > 0) {
			AuctionParticipantDTO winner = auctionDAOLocal.findWinner(lAuction
					.getId());
			String sellersMessage = message.createSellerMessage(lAuction, winner); // get
			// message
			// to
			// send
			// to
			// seller
			String winnersMessage = message.createWinnerMessage(lAuction, winner);// get
			// message
			// to
			// send
			// to
			// winner
			
			
			emailSeller.sendMail("bidon090814@gmail.com", lAuction
					.getAuctionparticipant().getEmail(), "Auction is over",
					sellersMessage);

			emailWinner.sendMail("bidon090814@gmail.com", auctionDAO
					.findWinner(lAuction.getId()).getEmail(),
					"You are the winner", winnersMessage);

			List<Bid> bidders = bidDAOLocal
					.getBiddersOfAuction(auction.getId());
			for (Bid bid : bidders) {

				String bidderMessage = message.createAllBiddersMessage(auction,
						bid, winner);

				emailBidders.sendMail("bidon090814@gmail.com", bid
						.getAuctionparticipant().getEmail(),
						"Auction has finished", bidderMessage);
			}

		}

		else {
			lAuction.setStatus(AuctionStatus.PENDING);
			emailSeller.sendMail("bidon090814@gmail.com", lAuction
					.getAuctionparticipant().getEmail(),
					"Your auction is over.", message
							.createNoBidsMessage(lAuction));
		}

		System.out.println(auction.toString());

		timer.cancel();
	}

	/**
	 * @inheritDoc
	 */
	public void stopTimer(Serializable info) {
		Collection<Timer> timers = timerService.getAllTimers();
		for (Timer time : timers) {
			if (time.getInfo().equals(info)) {
				time.cancel();
				break;
			}
		}
	}

	public void killTimer(Integer value) {
		Timer timer = TimerUtils.getTimer(value);
		if (timer != null)
			timer.cancel();
	}

}
