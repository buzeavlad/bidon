/**
 * 
 */
package utils;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.Timer;

import model.Auction;

/**
 * @author Cipri
 *
 */
public class TimerUtils {

	static ObjectMapper<Integer, Timer> map=new ObjectMapper<Integer, Timer>();


	public static void addAuction(Timer t, Auction a) {
		map.addMapping(a.getId(), t);
	}
	
	public static Integer getAuction(Timer t){
		return map.getKey(t);
	}
	
	public static Timer getTimer(Integer value){
		return map.getObject(value);
	}
	
	
	
	
}
