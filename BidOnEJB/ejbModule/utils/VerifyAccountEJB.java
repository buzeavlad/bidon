package utils;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import daos.AuctionParticipantDAORemote;
import dto.AuctionParticipantDTO;

/**
 * This class implements the methods to accept or reject a user.
 * 
 * @author Stefi Marcu
 */

@Stateless
@LocalBean
public class VerifyAccountEJB implements VerifyAccountEJBRemote {

	@EJB
	AuctionParticipantDAORemote auctionParticipantDAORemote;

	private EmailManager emailManager;

	private EmailMessage message;

	/**
	 * Calls the update method from AuctionParticipantDAO and it changes the
	 * user's status from 'PENDING' to 'VALID'. An email with the newly created
	 * account details is sent to the user, together with a link which redirects
	 * him to the login page.
	 * 
	 * @param accountUsername
	 */
	@Override
	public void acceptAccount(String accountUsername) {

		AuctionParticipantDTO auctionParticipantDTO = auctionParticipantDAORemote
				.findByUsername(accountUsername);
		auctionParticipantDTO.setStatus(AuctionParticipantDTO.VALID);
		int id = auctionParticipantDAORemote.findIdByUsername(accountUsername);

		auctionParticipantDAORemote.update(id, auctionParticipantDTO);

		emailManager = new EmailManager();
		message = new EmailMessage();

		emailManager.sendMail("bid090814@gmail.com",
				auctionParticipantDTO.getEmail(), "Account Validation",
				message.createLinkConfirmationMessage(auctionParticipantDTO));

	}

	/**
	 * Calls the delete method from AuctionParticipantDAO.
	 */

	@Override
	public void reject(String accountUsername) {

		auctionParticipantDAORemote.delete(accountUsername);

	}

}
