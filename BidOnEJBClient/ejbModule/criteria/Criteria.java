/**
 * 
 */
package criteria;

/**
 * @author Ildik�
 *	contains all the possible criteria by which to filter list of auctions.
 */
public class Criteria {
	
	private String productName;
	private String category; //name
	private String seller; //username
	private int minPrice;
	private int maxPrice;

	
	public Criteria() {
		this.productName = null;
		this.category = null;
		this.seller = null;
		this.minPrice = -1;
		this.maxPrice = -1;
	}


	/**
	 * @param productName
	 * @param category
	 * @param seller
	 */
	public Criteria(String productName, String category, String seller, int minPrice, int maxPrice) {
		super();
		this.productName = productName;
		this.category = category;
		this.seller = seller;
		this.minPrice = minPrice;
		this.maxPrice = maxPrice;
	}


	public String getProductName() {
		return productName;
	}


	public void setProductName(String productName) {
		this.productName = productName;
	}


	public String getCategory() {
		return category;
	}


	public void setCategory(String category) {
		this.category = category;
	}


	public String getSeller() {
		return seller;
	}


	public void setSeller(String seller) {
		this.seller = seller;
	}


	public int getMinPrice() {
		return minPrice;
	}


	public void setMinPrice(int minPrice) {
		this.minPrice = minPrice;
	}


	public int getMaxPrice() {
		return maxPrice;
	}


	public void setMaxPrice(int maxPrice) {
		this.maxPrice = maxPrice;
	}

}
