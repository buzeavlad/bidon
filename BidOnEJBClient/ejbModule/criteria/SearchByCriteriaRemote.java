package criteria;

import java.util.List;

import javax.ejb.Remote;

import dto.AuctionListDTO;

@Remote
public interface SearchByCriteriaRemote {
	
	/**
	 * finds running auctions by given criteria.
	 * @author Ildiko
	 * @param criteria - Criteria object, contains all the details by which to filter.
	 * @return list of auctionListDTOs that match the criteria
	 */
	List<AuctionListDTO> findByCriteria(Criteria criteria);
}
