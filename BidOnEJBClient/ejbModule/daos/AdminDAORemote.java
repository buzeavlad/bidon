package daos;

import java.util.List;

import javax.ejb.Remote;

import dto.AdminDTO;

/**
 * 
 * Interface that will be implemented by AdminDAO.
 * Defines basic operations for an AdminDAO
 * 
 * @author Zanc Razvan
 *
 */
@Remote
public interface AdminDAORemote {

	/**
	 * Inserts an Admin into our database
	 * 
	 * @param adminDTO
	 *            representing a basic Admin
	 * @return if the Admin was successfully inserted into our database
	 */
	public boolean create(AdminDTO adminDTO);

	/**
	 * Finds all Admins
	 * 
	 * @return List of all Admins
	 */
	public List<AdminDTO> findAll();

	/**
	 * Performs an update on an Admin
	 * 
	 * @param id
	 *            -> Admin's id from database
	 * @param adminDTO
	 *            -> the new Admin representing a basic Admin
	 * @return if the update was successfully finished
	 */
	public boolean update(int id, AdminDTO adminDTO);

	/**
	 * Deletes an Admin from our database using it's username
	 * 
	 * @param username
	 * @return if the Admin was deleted
	 */
	public boolean delete(String username);

	/**
	 * Finds an Admin using it's username
	 * 
	 * @param username
	 * @return the Admin
	 */
	public AdminDTO findByUsername(String username);

	/**
	 * Finds the id of an Admin ( MUST USE BEFORE UPDATE ! )
	 * 
	 * @param username
	 * @return id -> if their is an id associated to the username given as
	 *         parameter else -1
	 */
	public int findIdByUsername(String username);
}