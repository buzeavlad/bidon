package daos;

import java.util.List;

import javax.ejb.Remote;

import dto.AuctionDTO;
import dto.AuctionListDTO;
import dto.AuctionParticipantDTO;
import dto.BidDTO;

  
/**
 * 
 * Interface that will be implemented by AuctionDAO.
 * Defines basic operations for AuctionDAO
 * 
 * @author Zanc Razvan
 *
 */ 
@Remote
public interface AuctionDAORemote {
		
	public boolean update(int id, AuctionDTO auctionDTO);
	
	public List<AuctionListDTO> getWonAuctions(AuctionParticipantDTO auctPartDTO);
	
	public List<AuctionListDTO> getCreatedAuctions(AuctionParticipantDTO auctPartDTO);
	
	public  List<AuctionListDTO> getOnGoingAuction();

	/**
	 * sets the status of given auction to finished.
	 * @author Ildiko
	 * @param id - id of given auction.
	 * @return true if update was successful, false otherwise
	 */
	boolean finishAuction(int id);
	
	
	/**
	 * finds an auctionDTO by its id.
	 * @author Ildiko
	 * @param id - id of auction
	 * @return AuctionDTO if auction was found, null otherwise
	 */
	public AuctionDTO findById(int id);
	
	/**
	 * finds the participant with the highest bid for given auction.
	 * @author Ildiko
	 * @param auctionId - id of auction
	 * @return AuctionParticipantDTO, winner of the auction
	 */
	public AuctionParticipantDTO findWinner(int auctionId);
	
	/**
	 * 
	 * @author Vlad
	 * @param idAuction
	 * @return Max Bid from Auction with id = idAuction
	 */
	public BidDTO getLastBidDTO(int idAuction);
	
	/**
	 * Function used to check if winnerUsername has won at least one auction of sellerUsername
	 * @param sellerUsername potential seller
	 * @param winnerUsername potential winner
	 * @return
	 */
	public boolean hasWonAuction(String sellerUsername,String winnerUsername);
	
	public boolean cancelAuction(int id);

}
