package daos;

import java.util.List;

import javax.ejb.Remote;

import dto.AuctionParticipantDTO;
import dto.UserDTO;

/**
 * 
 * Interface that will be implemented by AdminDAO Defines basic operations for
 * an AdminDAO
 * 
 * @author Zanc Razvan
 *
 */
@Remote  
public interface AuctionParticipantDAORemote {

	/**
	 * Method used to punish a user when he cancels an auction
	 * 
	 * @param username
	 */
	public void applyPentalies(String username);

	/**
	 * Inserts an AuctionParticipant into our database
	 * 
	 * @param auctionDTO
	 *            representing a basic AuctionParticipant
	 * @return if the AuctionParticipant was successfully inserted into our
	 *         database
	 */
	public boolean create(AuctionParticipantDTO auctionDTO);

	/**
	 * Finds all AuctionParticipants with PENDING as status
	 * 
	 * @return List of AuctionParticipants with PENDING as status
	 */
	public List<AuctionParticipantDTO> allPendingAccounts();

	/**
	 * Performs an update on an ActionParticipant
	 * 
	 * @param id
	 *            -> AuctionParticipant's id from database
	 * @param auctionDTO
	 *            -> the new AuctionParticipant representing a basic
	 *            AuctionParticipant
	 * @return if the update was successfully finished
	 */
	public boolean update(int id, AuctionParticipantDTO auctionDTO);



	/**
	 * Deletes an AuctionParticipant from our database using it's username
	 * 
	 * @param username
	 * @return if the AuctionParticipant was deleted
	 */
	public boolean delete(String username);

	/**
	 * Finds an AuctionParticipant using it's username
	 * 
	 * @param username
	 * @return the AuctionParticipant
	 */
	public AuctionParticipantDTO findByUsername(String username);

	/**
	 * Finds an AuctionParticipant using it's email
	 * 
	 * @param email
	 * @return the AuctionParticipant
	 */
	public AuctionParticipantDTO findByEmail(String email);

	/**
	 * Finds the id associated with the username given as parameter MUST BE USED
	 * BEFORE UPDATE
	 * 
	 * @param username
	 * @return the AuctionParticipant
	 */
	public int findIdByUsername(String username);

	/**
	 * Find the auctionParticipant with the given id;
	 * 
	 * @param id
	 * @return the AuctionParticipant
	 */
	public AuctionParticipantDTO findByID(int id);

	/**
	 * Finds the id associated with the email address given as parameter
	 * 
	 * @param email
	 * @return the AuctionParticipant
	 * @author Carina Paul
	 */
	public int findIdByEmail(String email);

	public boolean updateAndSendMail(int id, UserDTO userDTO);

}
