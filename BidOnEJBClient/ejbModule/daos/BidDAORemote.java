package daos;

import java.util.List;

import javax.ejb.Remote;

import dto.BidDTO;

/**
 * 
 * Interface that will be implemented by BidDAO.
 * Defines basic operations for BidDAO
 * 
 * @author Zanc Razvan
 *  
 */
@Remote
public interface BidDAORemote {
	
	
	/**
	 * Places a bid to an auction
	 * 
	 * @param bidDTO
	 *            representing a basic Bid
	 * @return if the Bid was successfully inserted into our database
	 */
	public boolean create(BidDTO bidDTO);
	
	
	/**
	 * Finds all Bids
	 * 
	 * @return List of all Bids
	 */
	
	public List<BidDTO> findAllBidsFromAnAuction(int idAuction); 
	
	/**
	 * Return the maximum amount bidded on an auction.
	 * If no bids, then returns 0;
	 * @author gabriel.horgos
	 * @param id : int Auction id
	 * @return : int Max amount bidded
	 */
	public int getCurrentPrice(int id);

	/**
	 * finds the max bid for given auction.
	 * @param auctionId - id of auction.
	 * @return BidDTO, highest bid of auction
	 */
	public BidDTO findMaxBidDTO(int auctionId);
	
	
}
