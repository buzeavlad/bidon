package daos;

import java.util.List;

import javax.ejb.Remote;

import dto.CategoryDTO;
import dto.UserDTO;

@Remote
public interface CategoryDAORemote {

	/**
	 * Method that insert a category in Database
	 * 
	 * @param categoryDTO- category that will be inserted in database
	 * @return true if the insert is successful
	 */
	boolean create(CategoryDTO categoryDTO);
	
	

	/**
	 * Updates the name of a category in database
	 * 
	 * @param id -  the id of a category
	 * @param categoryDTO - the category that will replace the one existing in database 
	 * @return true if the update is successful
	 */
	boolean update(int id, CategoryDTO categoryDTO);
	
	
	/**
	 * Delete a category by name
	 * 
	 * @param name of category
	 * @return true if the delete is successful
	 */
	boolean delete(String name);

	/**
	 * Select the category by name
	 * 
	 * @param name of category
	 * @return the category with the name: name
	 */
	CategoryDTO findByName(String name);
	
	/**
	 * Ildiko
	 *
	 * @return
	 */
	List<CategoryDTO> findAll();

}
