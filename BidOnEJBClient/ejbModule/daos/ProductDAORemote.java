package daos;

import java.util.List;

import javax.ejb.Remote;

import dto.AuctionDTO;
import dto.ProductDTO;

/**
 * Interface for basic Database operations for Product
 * @author Cipri
 *
 */
@Remote
public interface ProductDAORemote {

	/**
	 * 
	 * The method that insert a product in Database
	 * @param productDTO -the product that will be insert
	 * @return true if the inseration works
	 */
	boolean create(ProductDTO productDTO);

	/**
	 * Method that updates a Product in Database
	 * 
	 * @param id -the id of a project from database
	 * @param productDTO the Product that will be updated in DataBase
	 * @return if update works
	 * 
	 */
	boolean update(int id, ProductDTO productDTO);

	/**
	 * List all the product in the database
	 * @return the list of products
	 */
	List<ProductDTO> findAll();

	
	public ProductDTO findByID(int idProduct);

}
