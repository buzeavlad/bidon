package daos;

import java.util.List;

import javax.ejb.Remote;

import dto.ReviewDTO;

/**
 * Interface that describes the behavior of a Review in dataBase
 * @author Cipri
 *
 */
@Remote
public interface ReviewDAORemote {

	/**
	 * Creates a new Review
	 * 
	 * @param reviewDTO - the Review that will be added in DataBase
	 * @return true if successful
	 */
	boolean create(ReviewDTO reviewDTO);

	/**
	 * Method that lists all the reviews from dataBase
	 * 
	 * @return the list of reviews 
	 */
	List<ReviewDTO> findALL();

	/**
	 * Method that updates a review in database
	 * 
	 * @param id of the review
	 * @param reviewDTO -the review that will be replaced in database
	 * @return
	 */
	boolean update(int id, ReviewDTO reviewDTO);

	/**
	 * Lists all the Reviews of a user, 
	 * 
	 * @param id the user's id
	 * @return the list of reviews
	 */
	List<ReviewDTO> findReviewByUser(int id);

}
