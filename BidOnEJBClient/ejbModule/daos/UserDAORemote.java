package daos;

import javax.ejb.Remote;

import dto.UserDTO;
/**
 * 
 * @author Admin
 *
 */
@Remote
public interface UserDAORemote {
	
	public boolean create(UserDTO userDTO);
	
	public boolean update(int id, UserDTO userDTO);
	
	public boolean updateAndSendMail(int id, UserDTO userDTO);
	
	public boolean delete(String username); 

	public UserDTO findByUsername(String username);
	
	public UserDTO findByEmail (String email);
	
	public int findIdByUsername (String username);
    	
}
