package dto;
/**
 * 
 * @author bogdan.moldovan....
 *...
 */
public class AdminDTO extends UserDTO {
	private int autorityLevel;
	
	
	/**
	 * Constructor for AdminDTO
	 */
	public AdminDTO() {
	}
	
	public AdminDTO(String email, String firstName, String lastName,
			String password, String username,int autorityLevel) {
		super(email, firstName, lastName, password, username);
		this.autorityLevel = autorityLevel;
	}

	/**
	 * Getters and setters for AdminDTO
	 * @author Stefi Marcu
	 * 
	 */
	public int getAutorityLevel() {
		return autorityLevel;
	}

	public void setAutorityLevel(int autorityLevel) {
		this.autorityLevel = autorityLevel;
	}

	@Override
	public String toString() {
		return super.toString()+"\n authority level = "+autorityLevel;
	}
	
	

}
