/**
 * 
 */
package dto;

import java.util.Date;

/**
 * @author Ildik�
 *
 */
public class AuctionDTO {
	
	public static String RUNNING = "RUNNING";
	public static String PENDING = "PENDING";
	public static String FINISHED = "FINISHED";
	
	private int id;
	private int idSeller;
	private int startingPrice;
	private int incrementRate;
	private Date dueDate;
	private int idProduct;
	private String status;
	/**
	 * 
	 */
	public AuctionDTO() {

	}
	/**
	 * @param id
	 * @param seller
	 * @param startingPrice
	 * @param incrementRate
	 * @param dueDate
	 * @param product
	 * @param status
	 */
	public AuctionDTO(int id, int seller, int startingPrice,
			int incrementRate, Date dueDate, int product, String status) {
		super();
		this.id = id;
		this.idSeller = seller;
		this.startingPrice = startingPrice;
		this.incrementRate = incrementRate;
		this.dueDate = dueDate;
		this.idProduct = product;
		this.status = status;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public int getStartingPrice() {
		return startingPrice;
	}
	public void setStartingPrice(int startingPrice) {
		this.startingPrice = startingPrice;
	}
	public int getIncrementRate() {
		return incrementRate;
	}
	public void setIncrementRate(int incrementRate) {
		this.incrementRate = incrementRate;
	}
	public Date getDueDate() {
		return dueDate;
	}
	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int getIdSeller() {
		return idSeller;
	}
	public void setIdSeller(int idSeller) {
		this.idSeller = idSeller;
	}
	public int getIdProduct() {
		return idProduct;
	}
	public void setIdProduct(int idProduct) {
		this.idProduct = idProduct;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
		
}
