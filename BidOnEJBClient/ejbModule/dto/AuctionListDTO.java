package dto;

import java.util.Date;
import java.sql.Timestamp;

public class AuctionListDTO {

	private String productName;
	private Date dueDate;
	private int currentPrice;
	private int startingPrice;
	private String sellerUsername;
	private int id;
	private String status;
	public AuctionListDTO() {
		
	}
	
	public AuctionListDTO(String productName, Date dueDate,
			int currentPrice, int startingPrice, String username,int id) {
		super();
		this.productName = productName;
		this.dueDate = dueDate;
		this.currentPrice = currentPrice;
		this.startingPrice = startingPrice;
		this.sellerUsername = username;
		this.id = id;
	}

	/**
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}
	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}
	
	/**
	 * @return the price
	 */
	public int getPrice() {
		return currentPrice;
	}
	/**
	 * @return the dueDate
	 */
	public Date getDueDate() {
		return dueDate;
	}

	/**
	 * @param dueDate the dueDate to set
	 */
	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	/**
	 * @return the currentPrice
	 */
	public int getCurrentPrice() {
		return currentPrice;
	}

	/**
	 * @param currentPrice the currentPrice to set
	 */
	public void setCurrentPrice(int currentPrice) {
		this.currentPrice = currentPrice;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(int price) {
		this.currentPrice = price;
	}
	/**
	 * @return the startingPrice
	 */
	public int getStartingPrice() {
		return startingPrice;
	}
	/**
	 * @param startingPrice the startingPrice to set
	 */
	public void setStartingPrice(int startingPrice) {
		this.startingPrice = startingPrice;
	}
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the sellerUsername
	 */
	public String getSellerUsername() {
		return sellerUsername;
	}

	/**
	 * @param sellerUsername the sellerUsername to set
	 */
	public void setSellerUsername(String sellerUsername) {
		this.sellerUsername = sellerUsername;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AuctionListDTO [productName=" + productName + ", dueDate="
				+ dueDate + ", currentPrice=" + currentPrice
				+ ", startingPrice=" + startingPrice + ", sellerUsername="
				+ sellerUsername + ", id=" + id + "]";
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	
	
	
	
	
}
