package dto;



public class AuctionParticipantDTO extends UserDTO {
	
	public static final String PENDING="PENDING";
	public static final String VALID="VALID";
	
	private String address;

	private int bidcoins;

	private String phone;
	
	private String status;
	
	private boolean subscribedForEmail;
	
	public AuctionParticipantDTO(){
		
	}

	public AuctionParticipantDTO(String email, String firstName,
			String lastName, String password, String username,String address,int bidcoins,String phone, boolean subscribedForEmail,String status) {
		super(email, firstName, lastName, password, username);
		this.address=address;
		this.bidcoins=bidcoins;
		this.phone=phone;
		this.subscribedForEmail=subscribedForEmail;
		this.status = status;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the bidcoins
	 */
	public int getBidcoins() {
		return bidcoins;
	}

	/**
	 * @param bidcoins the bidcoins to set
	 */
	public void setBidcoins(int bidcoins) {
		this.bidcoins = bidcoins;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	public boolean isSubscribedForEmail() {
		return subscribedForEmail;
	}

	public void setSubscribedForEmail(boolean subscribedForEmail) {
		this.subscribedForEmail = subscribedForEmail;
	}

}
