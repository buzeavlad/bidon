package dto;

import java.util.Date;




public class BidDTO {
	private int amount;

	private Date date;

	private int idAuction;
	
	private int idBidder;
	
	
	/**
	 * 
	 */
	public BidDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * @param amount
	 * @param date
	 * @param auction
	 * @param bidder
	 */
	public BidDTO(int amount, Date date, int auction,
			int bidder) {
		super();
		this.amount = amount;
		this.date = date;
		this.idAuction = auction;
		this.idBidder = bidder;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getIdAuction() {
		return idAuction;
	}

	public void setIdAuction(int idAuction) {
		this.idAuction = idAuction;
	}

	public int getIdBidder() {
		return idBidder;
	}

	public void setIdBidder(int idBidder) {
		this.idBidder = idBidder;
	}

}
