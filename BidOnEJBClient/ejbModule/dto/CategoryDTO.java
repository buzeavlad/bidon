package dto;

public class CategoryDTO {
	private String name;

	public CategoryDTO(String name) {
		super();
		this.name = name;
	}

	public CategoryDTO() {
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "CategoryDTO [name=" + name + "]";
	}
	
}
