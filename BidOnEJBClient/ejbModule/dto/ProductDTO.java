package dto;



public class ProductDTO {
	
	private String description;

	//private byte[] image;

	private String name;

	private String category;
	
	
	
	public ProductDTO() {

	}
	

	public ProductDTO(String description, String name, String category) {
		super();
		this.description = description;
		this.name = name;
		this.category = category;
	}


	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public String getCategory() {
		return category;
	}


	public void setCategory(String category) {
		this.category = category;
	}

	
	
}
