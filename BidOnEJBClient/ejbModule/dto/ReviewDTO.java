package dto;



public class ReviewDTO {
	private String description;

	private int rating;

	private int idAuction;

	
	public ReviewDTO() {
	}

	public ReviewDTO(String description, int rating, int auction) {
		this.description = description;
		this.rating = rating;
		this.idAuction = auction;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public int getIdAuction() {
		return idAuction;
	}

	public void setIdAuction(int idAuction) {
		this.idAuction = idAuction;
	}
	
}
