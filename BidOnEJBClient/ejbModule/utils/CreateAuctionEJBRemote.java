package utils;

import java.util.Date;

import javax.ejb.Remote;

import dto.AuctionDTO;
import dto.ProductDTO;

@Remote
public interface CreateAuctionEJBRemote {
	
	public void startAuction(Date date, AuctionDTO auctionDTO, ProductDTO productDTO);

	
	
}
