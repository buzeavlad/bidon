package utils;

import java.util.Date;

import javax.ejb.Remote;

import dto.AuctionDTO;

@Remote
public interface ExtendAuctionEJBRemote {
	public void extendAuction(Date date ,  AuctionDTO auctionDTO);
}
