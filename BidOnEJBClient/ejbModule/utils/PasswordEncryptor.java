package utils;

import java.security.MessageDigest;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * 
 * @author Cipri
 *
 */
public class PasswordEncryptor {
	
	private Logger LOGGER = Logger.getLogger(PasswordEncryptor.class
			.getName());

	/**
	 * 
	 * Encrypts the password in an hexadecimal String
	 * @param input -the String unencrypted
	 * @return the String for saving in database
	 */
	public  String encrypt(String input) {
		byte[] output = null;
		try {
			MessageDigest md = MessageDigest.getInstance("SHA1");

			md.update(input.getBytes());
			output = md.digest();

		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Error Encryption");
			e.printStackTrace();
		}

		return bytesToHex(output);
	}

	private  String bytesToHex(byte[] b) {
		char hexDigit[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
				'A', 'B', 'C', 'D', 'E', 'F' };
		StringBuffer buf = new StringBuffer();
		
		for (int j : b){
			buf.append(hexDigit[(j >> 4) & 0x0f]);
			buf.append(hexDigit[j & 0x0f]);
		}
		return buf.toString();
	}


}
