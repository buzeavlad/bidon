/**
 * 
 */
package utils;

import java.io.Serializable;
import java.util.Date;

import javax.ejb.Remote;
import javax.ejb.Timer;

/**
 * @author Cipri
 *
 */
@Remote
public interface TimerSessionEJBRemote {

	/**
	 * The method that will be called when the timer is stopped 
	 * 
	 * @param timer the timer which will stop
	 */
	public void updateDB(Timer timer);

	/**
	 * The method that will kill all the timers in progress
	 * 
	 * @param info
	 */
	public void stopTimer(Serializable info);

}
