package utils;

/**
 * autoarea e ildiko
 */
  
import javax.naming.InitialContext;
import javax.naming.NamingException;

import daos.AuctionParticipantDAORemote;
import dto.AuctionParticipantDTO;

public class Validator {
		

	/**
	 * validates an AuctionParticipantDTO (username, password - two versions, first name, last name, email, phone, address)
	 *
	 * @param user - AuctionParticipantDTO, the user registering
	 * @param repass - the retyped password
	 * @return true if user is valid
	 * @throws Exception if user is not valid.
	 */

	public static boolean validate(AuctionParticipantDTO user, String repass) throws Exception{
		Validator val = new Validator();
		if (val.validateUsername(user.getUsername())==false){
			throw new Exception("Username incorrect.");
		}
		if (val.validatePassword(user.getPassword(), repass)==false){
			throw new Exception("Password incorrect.");
		}
		if (val.validateFirstName(user.getFirstName())==false){
			throw new Exception("First name incorrect.");
		}
		if (val.validateLastName(user.getLastName())==false){
			throw new Exception("Last name incorrect.");
		}
		if (val.validateEmail(user.getEmail())==false){
			throw new Exception("Email incorrect.");
		}
		if (val.validatePhone(user.getPhone())==false){
			throw new Exception("Phone number incorrect.");
		}
		if (val.validateAddress(user.getAddress())==false){
			throw new Exception("Address incorrect.");
		}
		return true;
	}
	
	/**
	 * Checks that username is not null, not an empty string, unique
	 * @param username  
	 * @return false if not valid, true otherwise
	 */
	private boolean validateUsername(String username){
		if (username.equals(null))
			return false;
		if (username.equals(""))
			return false;
		
		// TODO test
		InitialContext icx;
		try {
			icx = new InitialContext();
			AuctionParticipantDAORemote apr = (AuctionParticipantDAORemote) icx.lookup("java:global/BidOnEAR/BidOnEJB/AuctionParticipantDAO!daos.AuctionParticipantDAORemote");

			AuctionParticipantDTO user = apr.findByUsername(username);
			
			if (user!=null){ 
				return false;
			}

		} catch (NamingException e) {
			e.printStackTrace();
		}
		
			
		return true;
	}
	


		/**
		 *checks that password is not null, not empty, if both versions are equal, if it contains at least one digit, an uppercase and a lowercase letter.
		 * @param password
		 * @param password1
		 * @return false if not valid, true otherwise
		 */
	private boolean validatePassword(String password, String password1){
		
		if (password==null){
			return false;
		}		
		if (password1==null){
			return false;
		}
		if (password.equals("")){
			return false;
		}
		if (password1.equals("")){
			return false;
		}
		if (!password.equals(password1)){
			return false;
		}
		if (!password.matches("(.*)\\d(.*)")){
			return false;
		}
		if (!password.matches("(.*)[a-z](.*)")){
			return false;
		}
		if (!password.matches("(.*)[A-Z](.*)")){
			return false;
		}
		return true;
	}	
		
	/**
	 * checks that first name is not null, not empty
	 *
	 * @param firstName
	 * @return false if not valid, true otherwise
	 */
	private boolean validateFirstName(String firstName){
		if (firstName==null){
			return false;
		}
		if (firstName.equals("")){
			return false;
		}
		return true;
	}
	
	/**
	 * checks that last name is not null, not empty
	 *
	 * @param lastName
	 * @return false if not valid, true otherwise
	 */
	private boolean validateLastName(String lastName){
		if (lastName==null){
			return false;
		}
		if (lastName.equals("")){
			return false;
		}
		return true;
	}

	/**
	 * checks that email is not null, not empty, and if it contains at least one '@' and one '.'
	 *
	 * @param email
	 * @return false if not valid, true otherwise
	 */
	private boolean validateEmail(String email){
		if (email==null){
			return false;
		}
		if (email.equals("")){
			return false;
		}
		if (!email.matches("(.+)@(.+)\\.(.+)")){
			return false;
		}
		
		// TODO test
		InitialContext icx;
		try {
			icx = new InitialContext();
			AuctionParticipantDAORemote apr = (AuctionParticipantDAORemote) icx.lookup("java:global/BidOnEAR/BidOnEJB/AuctionParticipantDAO!daos.AuctionParticipantDAORemote");

			AuctionParticipantDTO user = apr.findByEmail(email);
			
			if (user!=null){ 
				return false;
			}

		} catch (NamingException e) {
			e.printStackTrace();
		}
		return true;
	}
	
	/**
	 * checks that phone number only contains digits (at least one)
	 *
	 * @param phone
	 * @return false if not valid, true otherwise
	 */
	private boolean validatePhone(String phone){
		if (phone==null){
			return true;//?
		}
		if (!phone.matches("\\d*")){
			return false;
		}
		return true;
	}
	
	/**
	 * checks that address is not null, not empty
	 *
	 * @param address
	 * @return false if not valid, true otherwise
	 */
	private boolean validateAddress(String address){
		if (address==null){
			return false;
		}
		if (address.equals("")){
			return false;
		}
		return true;
	}
}
