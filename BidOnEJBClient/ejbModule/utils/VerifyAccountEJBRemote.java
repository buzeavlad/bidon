package utils;

import javax.ejb.Remote;

import dto.AuctionParticipantDTO;

@Remote
public interface VerifyAccountEJBRemote {
	
	void acceptAccount(String accountUsername);

	void reject(String accountUsername);


}
