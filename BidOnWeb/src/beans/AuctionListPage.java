package beans;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import criteria.Criteria;
import criteria.SearchByCriteriaRemote;
import daos.AuctionDAORemote;
import daos.BidDAORemote;
import dto.AuctionListDTO;
import dto.AuctionParticipantDTO;
import dto.BidDTO;

/**
 * This is the managed bean associated with the auctionListPage.xhtml It handles
 * the list of ongoing auctions.
 * 
 * @author gabriel.horgos
 *
 */
@ManagedBean(name = "auctionsListView")
public class AuctionListPage implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private AuctionDAORemote auctionDAORemote;

	private List<AuctionListDTO> auctionList;

	@ManagedProperty(value = "#{login}")
	private Login login;

	private AuctionParticipantDTO auctionParticipant;

	private Logger LOGGER = Logger.getLogger(AuctionListPage.class.getName());

	@EJB
	private SearchByCriteriaRemote search;

	String productName, category, seller, minPrice, maxPrice;

	Criteria criteria;

	@EJB
	BidDAORemote bidDAO;

	@PostConstruct
	public void init() {
		auctionParticipant = login.getAuctionParticipantDTO();

		if (auctionParticipant == null) {
			// User not logged in, redirect to login page.
			try {
				ExternalContext context = FacesContext.getCurrentInstance()
						.getExternalContext();
				context.redirect(context.getRequestContextPath()
						+ "/faces/login.xhtml");
			} catch (IOException e) {
				LOGGER.log(Level.INFO, "Redirect to login failed.");
				e.printStackTrace();
			}
		} else {

			Map<String, String> params = FacesContext.getCurrentInstance()
					.getExternalContext().getRequestParameterMap();
			String flag = params.get("search");

			if (flag != null && flag.equals("1")) {
				ExternalContext ec = FacesContext.getCurrentInstance()
						.getExternalContext();
				auctionList = (List<AuctionListDTO>) ec.getSessionMap().get(
						"auctionList");
			} else {
				auctionList = auctionDAORemote.getOnGoingAuction();
				
				for (int i=0; i<auctionList.size(); i++) {
					BidDTO bid = bidDAO.findMaxBidDTO(auctionList.get(i).getId());
					if (bid != null) {
						auctionList.get(i).setCurrentPrice(bid.getAmount());
					} else {
						auctionList.get(i).setCurrentPrice(auctionList.get(i).getStartingPrice());
					}
				}
			}
		}
	}

	/**
	 * @return the auctionList
	 */
	public List<AuctionListDTO> getAuctionList() {
		return auctionList;
	}

	public Login getLogin() {
		return login;
	}

	public void setLogin(Login login) {
		this.login = login;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getSeller() {
		return seller;
	}

	public void setSeller(String seller) {
		this.seller = seller;
	}

	public String getMinPrice() {
		return minPrice;
	}

	public void setMinPrice(String minPrice) {
		this.minPrice = minPrice;
	}

	public String getMaxPrice() {
		return maxPrice;
	}

	public void setMaxPrice(String maxPrice) {
		this.maxPrice = maxPrice;
	}

	public void searchAuction() {

		criteria = new Criteria();

		if (productName != null && !productName.equals("")) {
			criteria.setProductName(productName);
		}
		System.out.println("name " + productName);

		if (category != null && !category.equals("")) {
			criteria.setCategory(category);
		}
		System.out.println("categ " + category);

		if (seller != null && !seller.equals("")) {
			criteria.setSeller("seller " + seller);
		}
		System.out.println(seller);

		if (minPrice != null && !minPrice.equals("")) {
			criteria.setMinPrice(Integer.parseInt(minPrice));
		}
		System.out.println("minprice " +minPrice);

		if (maxPrice != null && !maxPrice.equals("")) {
			criteria.setMinPrice(Integer.parseInt(maxPrice));
		}
		System.out.println("maxprice" + maxPrice);

		auctionList = search.findByCriteria(criteria);
		ExternalContext ec = FacesContext.getCurrentInstance()
				.getExternalContext();
		ec.getSessionMap().put("auctionList", auctionList);
		ExternalContext context = FacesContext.getCurrentInstance()
				.getExternalContext();
		try {
			context.redirect(context.getRequestContextPath()
					+ "/faces/auctionListPage.xhtml?search=1");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Transmits the selected auction and redirects to the auctionDetails page.
	 * 
	 * @return : page address
	 */
	public String enterAuction() {
		Map<String, String> params = FacesContext.getCurrentInstance()
				.getExternalContext().getRequestParameterMap();

		int id = Integer.parseInt(params.get("id"));
		LOGGER.log(Level.INFO, "auction id in listPage = " + id);
		return "auctionDetails.xhtml?id=" + id;
	}
}
