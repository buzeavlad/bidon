/**
 * 
 */
package beans;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;

import daos.CategoryDAORemote;
import dto.CategoryDTO;

/**
 * @author Ildik�
 *
 */

@ManagedBean(name="categoryDummy")
public class CategoryDummy implements Serializable{


	private static final long serialVersionUID = 7518441721897240425L;

	private List<CategoryDTO> catList;
	
	@EJB
	private CategoryDAORemote categoryDAORemote;
	
	@PostConstruct
	public void init(){
		catList = categoryDAORemote.findAll();
	}
 
	public List<CategoryDTO> getCatList() {
		return catList;
	}

	public void setCatList(List<CategoryDTO> catList) {
		this.catList = catList;
	}
	
	
}
