/**
 * 
 */
package beans;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.event.FlowEvent;

import utils.CreateAuctionEJBRemote;
import daos.AuctionDAORemote;
import daos.AuctionParticipantDAORemote;
import dto.AuctionDTO;
import dto.CategoryDTO;
import dto.ProductDTO;

/**
 * @author Radu
 *
 */

@ManagedBean(name = "createAuction")
@ViewScoped
public class CreateAuction {
	private int idAuctionParticipant;
	private int product;
	private int incrementRate;
	private int startPrice;
	public Date date;
	public String description;
	public String productName;

	public String category;
	public List<CategoryDTO> categories;
	
	
//	InitialContext initialContext;

	@EJB
	private AuctionDAORemote auctionDAORemote;
	@EJB
	private CreateAuctionEJBRemote createAuctionEJB;
	@EJB 
	private AuctionParticipantDAORemote auctionParticipantDAO;

	/**
	 * 
	 */
	public CreateAuction() {

	}

	public int getProduct() {
		return product;
	}

	public void setProduct(int product) {
		this.product = product;
	}

	public int getIncrementRate() {
		return incrementRate;
	}

	public void setIncrementRate(int incrementRate) {
		this.incrementRate = incrementRate;
	}

	public int getStartPrice() {
		return startPrice;
	}

	public void setStartPrice(int startPrice) {
		this.startPrice = startPrice;
	}
	
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}
	
	
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String onFlowProcess(FlowEvent event) {
		return event.getNewStep();
	}

	public void create() {
		Map<String,String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		String username = params.get("username");
		System.out.println("Inainte de find by username" + username);
		idAuctionParticipant = auctionParticipantDAO.findIdByUsername(username); //trebuie luat user de pe sesiune !!!!!!
		System.out.println("Dupa find by username");
		AuctionDTO auctionDTO = new AuctionDTO();
		auctionDTO.setIdSeller(idAuctionParticipant);
		auctionDTO.setStartingPrice(startPrice);
		auctionDTO.setIncrementRate(incrementRate);
		auctionDTO.setDueDate(date);
		auctionDTO.setIdProduct(product);
		auctionDTO.setStatus("RUNNING");
		System.out.println("In create auction inainte de product");
		ProductDTO productDTO = new ProductDTO();
		productDTO.setCategory(category);
		productDTO.setName(productName);
		productDTO.setDescription(description);
		
		createAuctionEJB.startAuction(date, auctionDTO, productDTO);
		
		//timer.startTimer(date,auctionDTO);
		System.out.println("In create " +auctionDTO.toString());
		
		try {
			ExternalContext context = FacesContext.getCurrentInstance()
					.getExternalContext();
			context.redirect(context.getRequestContextPath()
					+ "/faces/homePage.xhtml");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
