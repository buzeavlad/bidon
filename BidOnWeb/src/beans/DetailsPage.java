package beans;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import daos.AuctionDAORemote;
import daos.AuctionParticipantDAORemote;
import daos.BidDAORemote;
import daos.ProductDAORemote;
import daos.ReviewDAORemote;
import dto.AuctionDTO;
import dto.AuctionParticipantDTO;
import dto.BidDTO;
import dto.ProductDTO;
import dto.ReviewDTO;
import util.Message;

/**
 * This is the managed bean associated with something.xhtml. It displays all the
 * details about a selected product and offers the possibility to place a bid on
 * it.
 * 
 * @author Stefi Marcu
 *
 */

@ManagedBean(name = "detailsPage")
@ViewScoped
public class DetailsPage {

	
	
	private int amount;

	private AuctionParticipantDTO auctionParticipant;

	private AuctionDTO auction;
	private AuctionParticipantDTO seller;
	private ProductDTO product;
	private int currentPrice;
	private int minBid;
	private BidDTO bid = new BidDTO();
	private Logger LOGGER = Logger.getLogger(AuctionListPage.class.getName());

	@EJB
	private BidDAORemote bidDAORemote;
	@EJB
	private AuctionDAORemote auctionDAORemote;
	@EJB
	private AuctionParticipantDAORemote auctionParticipantDAORemote;
	@EJB
	private ProductDAORemote productDAORemote;

	@ManagedProperty(value = "#{login}")
	private Login login;
	
	private String auctionID;
	private String infoMessage;
	
	/**
	 * All the reviews corresponding to the user(name)
	 */
	private List<ReviewDTO> reviews;
	
	/**
	 * Business logic implementations for Reviews
	 */
	@EJB
	private ReviewDAORemote reviewDAO;
	
	
	@PostConstruct
	public void init() {
		auctionParticipant = login.getAuctionParticipantDTO();
		String messID = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("message");
		if (messID != null) {
			infoMessage = Message.getMessage(messID);
		} else {
			infoMessage = "";
		}
		auctionID = getID();
		LOGGER.log(Level.INFO,"getID()=" + getID());
		if(auctionParticipant == null ) {
			//User not logged in, redirect to login page.
			try {
				ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
				context.redirect(context.getRequestContextPath() + "/faces/login.xhtml");
			} catch (IOException e) {
				LOGGER.log(Level.INFO,"Redirect to login failed.");
				e.printStackTrace();
			}
		} else {
			//find the auction selected
			LOGGER.log(Level.WARNING,"auction id is " + auctionID);
			auction = auctionDAORemote.findById(Integer.parseInt(auctionID));
			//find auction owner (seller)
			seller = auctionParticipantDAORemote.findByID(auction.getIdSeller());
			//find product on sale
			product = productDAORemote.findByID(auction.getIdProduct());
			if(bidDAORemote.getCurrentPrice(auction.getId()) == 0){
				currentPrice = auction.getStartingPrice();
				minBid = currentPrice;
			} else {
				currentPrice = bidDAORemote.getCurrentPrice(auction.getId());
				minBid = currentPrice + auction.getIncrementRate();
			}
			
			reviews = reviewDAO.findReviewByUser(auction.getIdSeller());
		}
	}
	
	private String getID() {
		return FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id");
	}

	/**
	 * Method used to place a bid
	 * 
	 * @return
	 */
	public void placeBid() {

	LOGGER.log(Level.INFO,"in placeBid()");	
	if(!auctionParticipant.getUsername().equals(seller.getUsername())) {
		if(auction.getStatus().equals("RUNNING")) {
			//No bids have been placed yet
			if(bidDAORemote.findAllBidsFromAnAuction(auction.getId()).size() == 0 ) {
				if(auctionParticipant.getBidcoins() >= amount && amount >= auction.getStartingPrice()) {
					//Create a new bid and update bidders coins.
					createBid();
					LOGGER.log(Level.INFO,"Bid has been created (DetailsPage.java).");
					redirectToDetailsPage("bidCreated");
					return;
				} else {
					//Bidcoins problem (first bidder)
					redirectToDetailsPage("insufficientBidCoins");
					return;
				}
				
			} else {
				//If last bidder is the current bidder, he can not bid.
				if(auctionParticipantDAORemote.findIdByUsername(auctionParticipant.getUsername())!=bidDAORemote.findMaxBidDTO(auction.getId()).getIdBidder()){
					if(auctionParticipant.getBidcoins() >= amount && amount >= minBid) {
						//Give the loser his money back
						BidDTO previousBid = bidDAORemote.findMaxBidDTO(auction.getId());
						AuctionParticipantDTO previousBidder = auctionParticipantDAORemote.findByID(previousBid.getIdBidder());
						previousBidder.setBidcoins(previousBidder.getBidcoins() + previousBid.getAmount());
						auctionParticipantDAORemote.update(previousBid.getIdBidder(), previousBidder);
						
						//create new bid
						createBid();
						LOGGER.log(Level.INFO,"Placed new bid.");
						redirectToDetailsPage("bidCreated");
						return;
					} else {
						redirectToDetailsPage("insufficientBidCoins");
						return;
						}
					} else {
						//One can not outbit himself.
						redirectToDetailsPage("outBidError");
						return;
					}
				}
			} else {
				//TO DO message=auction closed
				redirectToListPage();
				return;
			}
				
		}
	redirectToDetailsPage("bidderConflict");
	return;
	
	}

	/**
	 * Creates a new Bid for selected auction.
	 */
	public void createBid() {
		BidDTO bid = new BidDTO();
		bid.setAmount(amount);
		Date date = new Date(System.currentTimeMillis());
		bid.setDate(date);
		bid.setIdAuction(auction.getId());
		int bidderId = auctionParticipantDAORemote.findIdByUsername(auctionParticipant.getUsername());
		bid.setIdBidder(bidderId);
		auctionParticipant.setBidcoins(auctionParticipant.getBidcoins() - amount);
		auctionParticipantDAORemote.update(bidderId, auctionParticipant);
		bidDAORemote.create(bid);
	}
	
	/**
	 * Redirects to auctionDetails.xhtml
	 */
	public void redirectToDetailsPage(String message) {
		try {
			ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();	
			context.redirect(context.getRequestContextPath() + "/faces/auctionDetails.xhtml?id=" + auctionID + "&message=" + message);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	
	/**
	 * Redirects to auctionListPage.xhtml
	 */
	public void redirectToListPage() {
		try {
			ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();	
			context.redirect(context.getRequestContextPath() + "/faces/auctionListPage.xhtml");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	//Setters and getters
	public AuctionParticipantDTO getSeller() {
		return seller;
	}

	public void setSeller(AuctionParticipantDTO seller) {
		this.seller = seller;
	}

	public int getCurrentPrice() {
		return currentPrice;
	}

	public void setCurrentPrice(int currentPrice) {
		this.currentPrice = currentPrice;
	}

	public int getMinBid() {
		return minBid;
	}

	public void setMinBid(int minBid) {
		this.minBid = minBid;
	}

	public BidDTO getBid() {
		return bid;
	}

	public void setBid(BidDTO bid) {
		this.bid = bid;
	}
	public String getAuctionID() {
		return auctionID;
	}

	public void setAuctionID(String auctionID) {
		this.auctionID = auctionID;
	}

	public Login getLogin() {
		return login;
	}

	public void setLogin(Login login) {
		this.login = login;
	}

	public ProductDTO getProduct() {
		return product;
	}

	public void setProduct(ProductDTO product) {
		this.product = product;
	}

	public AuctionDTO getAuction() {
		return auction;
	}

	public void setAuction(AuctionDTO auction) {
		this.auction = auction;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public AuctionParticipantDTO getAuctionParticipant() {
		return auctionParticipant;
	}

	public void setAuctionParticipant(AuctionParticipantDTO auctionParticipant) {
		this.auctionParticipant = auctionParticipant;
	}

	public String getInfoMessage() {
		return infoMessage;
	}

	public void setInfoMessage(String infoMessage) {
		this.infoMessage = infoMessage;
	}

	/**
	 * @return the reviews
	 */
	public List<ReviewDTO> getReviews() {
		return reviews;
	}

	/**
	 * @param reviews the reviews to set
	 */
	public void setReviews(List<ReviewDTO> reviews) {
		this.reviews = reviews;
	}
	
}