package beans;

import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import utils.PasswordEncryptor;
import daos.AuctionParticipantDAORemote;
import dto.AuctionParticipantDTO;

/**
 * Managed bean that handles user profile editing and password changing
 * 
 * @author Bogdan Moldovan
 *
 */
@ManagedBean(name = "editProfile")
@ViewScoped
public class EditProfile {

	/**
	 * The user set on session when logging in Is null only if not logged in
	 */
	AuctionParticipantDTO user;

	/**
	 * Creates the user in database
	 */
	@EJB
	AuctionParticipantDAORemote userDAO;

	/**
	 * Stores the logged in user
	 */
	@ManagedProperty(value = "#{login}")
	private Login login;

	/**
	 * Encrypts the password
	 */
	PasswordEncryptor passwordEncryptor;

	/**
	 * Fields related to address
	 */
	private String country, city, street, streetNo;

	/**
	 * Fields related to password changing
	 */
	private String oldPassword, newPassword, confirmPassword;

	/**
	 * Gets the user from session, initializes variables and processes the
	 * address
	 */
	@PostConstruct
	public void init() {
		user = login.getAuctionParticipantDTO();

		if (user == null) {
			ExternalContext context = FacesContext.getCurrentInstance()
					.getExternalContext();
			try {
				context.redirect(context.getRequestContextPath()
						+ "/faces/login.xhtml");
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {

			passwordEncryptor = new PasswordEncryptor();

			String adr = user.getAddress();
			String[] a = adr.split(", ");
			country = a[0];
			city = a[1];
			street = a[2];
			streetNo = a[3];
		}
	}

	public AuctionParticipantDTO getUser() {
		return user;
	}

	public void setUser(AuctionParticipantDTO user) {
		this.user = user;
	}

	public Login getLogin() {
		return login;
	}

	public void setLogin(Login login) {
		this.login = login;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getStreetNo() {
		return streetNo;
	}

	public void setStreetNo(String streetNo) {
		this.streetNo = streetNo;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	/**
	 * Edit user profile
	 * 
	 * @return destination page
	 */
	public String editProfile() {
		if (user != null) {
			user.setUsername(user.getUsername());
			user.setFirstName(user.getFirstName());
			user.setLastName(user.getLastName());
			user.setEmail(user.getEmail());
			user.setPhone(user.getPhone());
			user.setAddress(country + ", " + city + ", " + street + ", "
					+ streetNo);
			user.setBidcoins(user.getBidcoins());

			int id = userDAO.findIdByUsername(user.getUsername());
			userDAO.update(id, user);

			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_INFO, "Succes:",
							"Profile succesfully updated"));

			return "editProfile.xhtml";
		} else {

			ExternalContext context = FacesContext.getCurrentInstance()
					.getExternalContext();
			try {
				context.redirect(context.getRequestContextPath()
						+ "/faces/login.xhtml");
			} catch (IOException e) {
				e.printStackTrace();
			}
			return "login.xhtml";
		}
	}

	/**
	 * Change user password
	 * 
	 * @return destination page
	 */
	public String changePassword() {
		if (user != null) {
			if (user.getPassword().equals(
					passwordEncryptor.encrypt(oldPassword))) {

				if (!newPassword.matches("(.*)\\d(.*)")
						|| !newPassword.matches("(.*)[a-z](.*)")
						|| !newPassword.matches("(.*)[A-Z](.*)")) {
					FacesContext
							.getCurrentInstance()
							.addMessage(
									null,
									new FacesMessage(
											FacesMessage.SEVERITY_ERROR,
											"Password incorrect.",
											"Password needs to contain at least one uppercase character, one lowercase character and one digit."));
				} else {
					user.setPassword(passwordEncryptor.encrypt(newPassword));

					int id = userDAO.findIdByUsername(user.getUsername());
					userDAO.update(id, user);

					FacesContext.getCurrentInstance().addMessage(
							null,
							new FacesMessage(FacesMessage.SEVERITY_INFO,
									"Succes:", "Password succesfully changed"));
				}

				return "editProfile.xhtml";
			} else {
				FacesContext.getCurrentInstance().addMessage(
						null,
						new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error:",
								"Old Password does not match"));

				return "editProfile.xhtml";
			}
		} else {
			redirectToLogin();
			
			return "login.xhtml";
		}
	}

	public void redirectToLogin() {
		ExternalContext context = FacesContext.getCurrentInstance()
				.getExternalContext();
		try {
			context.redirect(context.getRequestContextPath()
					+ "/faces/login.xhtml");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
