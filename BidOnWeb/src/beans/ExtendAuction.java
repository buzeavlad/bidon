package beans;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.io.Serializable;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;

import utils.ExtendAuctionEJBRemote;
import daos.AuctionDAORemote;
import daos.AuctionParticipantDAORemote;
import dto.AuctionDTO;

/**
 * 
 * @author Radu this is a managed bean that extends the time of an ended auction
 *
 */
@ManagedBean(name = "extendAuction", eager = true)
@SessionScoped
public class ExtendAuction implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ExtendAuction() {
		// TODO Auto-generated constructor stub
	}

	@ManagedProperty(value = "#{hidden1}")
	private String passedUrlID;



	public String getPassedUrlID() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		passedUrlID = (String) facesContext.getExternalContext()
				.getRequestParameterMap().get("id");

		System.out.println("id din url : " + passedUrlID);

		return passedUrlID;
	}

	public void setPassedUrlID(String passedParameter) {
		passedUrlID = passedParameter;
		System.out.println("id in hidden : " + passedUrlID);
	}

	@ManagedProperty(value = "#{dateStr}")
	private String dateStr;

	public Date date;

	public String getDateStr() {
		return dateStr;
	}

	public void setDateStr(String dateStr) {
		this.dateStr = dateStr;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@EJB
	private AuctionDAORemote auctionDAORemote;
	@EJB
	private AuctionParticipantDAORemote auctionParticipantDAORemote;
	@EJB
	private ExtendAuctionEJBRemote extendAuctionEJB;

	private AuctionDTO auctionDTO;

	public String extend() {

		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		Date currentDate = new Date();
		System.out.println(dateFormat.format(currentDate));
		System.out.println(dateFormat.format(date));
		int idFromHidden = Integer.parseInt(passedUrlID);
		auctionDTO = auctionDAORemote.findById(idFromHidden);
		if (date.after(currentDate)) {
			auctionDTO.setDueDate(date);
			extendAuctionEJB.extendAuction(date, auctionDTO);
			System.out.println("EXTEEEENNNDDD: "+dateFormat.format(date)+passedUrlID);
		}
		return "homePage.xhtml";
	}

	public void onDateSelect(SelectEvent event) {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		facesContext.addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_INFO, "Date Selected",
						format.format(event.getObject())));
	}

	public void click() {
		RequestContext requestContext = RequestContext.getCurrentInstance();

		requestContext.update("form:display");
		requestContext.execute("PF('dlg').show()");
	}

	
}
