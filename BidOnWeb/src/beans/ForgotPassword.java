package beans;

import javax.annotation.PostConstruct;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.primefaces.event.FlowEvent;

import daos.AuctionParticipantDAORemote;
import dto.AuctionParticipantDTO;
import dto.UserDTO;

import java.io.IOException;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;





import utils.PasswordEncryptor;



/**
 * Managed bean necessary to be able to get a new password in case if the old
 * one was forgotten
 * 
 * @author Carina Paul
 *
 */
@ManagedBean(name = "forgotPassword")
@SessionScoped
public class ForgotPassword {

	/**
	 * Message to display in case of error.
	 */

	private String email;
	
	private AuctionParticipantDTO user;
	
	@EJB
	AuctionParticipantDAORemote userDAO;

	public String onFlowProcess(FlowEvent event) {
		return event.getNewStep();
	}
	
	public AuctionParticipantDTO getUser() {
		return user;
	}

	public void setUser(AuctionParticipantDTO user) {
		this.user = user;
	}
	
	

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}




	
	PasswordEncryptor passwordEncryptor;
	
	/**
	 * Function which generates a new password which consist of: 1 number,1
	 * capital letter and 4 minuscules generated randomly.
	 * 
	 * @return we get the pass parameter
	 */
	public String generatePswd() {

		String pass = "";

		Random ran = new Random();
		int number = ran.nextInt(9) + 0;

		pass = pass + number;

		for (int i = 0; i < 4; i++) {
			Random ran1 = new Random();
			char minuscule = (char) (ran1.nextInt(26) + 'a');
			pass = pass + minuscule;
		}

		Random ran1 = new Random();
		char letter = (char) (ran1.nextInt(26) + 'a');
		char capital = Character.toUpperCase(letter);

		pass = pass + capital;
		return pass;
	}
       
	/**
	 * Function used for update the new password in the DB in case if the e-mail address exists 
	 * In case of success sends a mail and returns a success.xhtml in other case return a failure.xhtml
	 * @return page navigation destination
	 */
	private Logger LOGGER = Logger.getLogger(AuctionListPage.class.getName());
	public void updateDB() {
		try{
			user=userDAO.findByEmail(email);
		} catch (Exception e) {
			System.out.println("Email does not exist");
		}
		if (user!= null) 
		{ 
				LOGGER.log(Level.INFO,"pass : " + generatePswd());
				user.setPassword(generatePswd());
				
				int id = userDAO.findIdByEmail(email);
		
				userDAO.updateAndSendMail(id, user);
		
				
			
				FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_INFO, "Succes:",
								"New Password successfully sent to you e-mail address!"));
				ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
				try {
					context.redirect(context.getRequestContextPath() + "/faces/login.xhtml");
					return;
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
			} else {
				FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error:",
								"There is no user registered with the given email address"));
				ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
				try {
					context.redirect(context.getRequestContextPath() + "/faces/forgotPassword.xhtml");
					return;
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		}

}