package beans;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import utils.PasswordEncryptor;
import daos.AdminDAORemote;
import daos.AuctionParticipantDAORemote;
import dto.AdminDTO;
import dto.AuctionParticipantDTO;

/**
 * This is the managed bean associated to Login.xhtml page.
 * It handles the login action.
 * @author gabriel.horgos
 *
 */
@ManagedBean(name="login")
@SessionScoped
public class Login {

	@EJB
	AuctionParticipantDAORemote auctionParticipantDAORemote;

	@EJB
	AdminDAORemote adminDAORemote;

	private AuctionParticipantDTO auctionParticipantDTO;
	private AdminDTO adminDTO;
	

	private PasswordEncryptor passwordEncryptor;
	private String username;
	private String password;

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username
	 * the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 * the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	
	/**
	 * @return the adminDTO
	 */
	public AdminDTO getAdminDTO() {
		return adminDTO;
	}
		
	/**
	 * @return the auctionParticipantDTO
	 */
	public AuctionParticipantDTO getAuctionParticipantDTO() {
		return auctionParticipantDTO;
	}

	/**
	 * Initialize passwordEncryptor
	 */
	@PostConstruct
	public void init(){
		passwordEncryptor = new PasswordEncryptor();
		auctionParticipantDTO=null;
	}

	/**
	 * Verifies login data for both admin and auctionParticipant
	 * @return "success" : if login data is valid.
	 * 
	 */
	public String login() {
		// Verify if password is valid and if user is an admin or an
		// auctionParticipant
		adminDTO = adminDAORemote.findByUsername(username);
		if (adminDTO != null && adminDTO.getPassword().equals(passwordEncryptor.encrypt(password))) {
			return "AccountValidation.xhtml";
		} else {
			adminDTO = null;
			auctionParticipantDTO = auctionParticipantDAORemote.findByUsername(username);
			if (auctionParticipantDTO != null && auctionParticipantDTO.getPassword().equals(passwordEncryptor.encrypt(password))
				&& auctionParticipantDTO.getStatus().equals("VALID")) {
				return "homePage.xhtml";				 
			} else {
				auctionParticipantDTO = null;
				return "login.xhtml";
			}
		}
	}

}
