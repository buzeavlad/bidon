package beans;

import java.io.IOException;

import javax.ejb.Stateless;
import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

/**
 * Session Bean implementation class Logout
 */
@Stateless
@ManagedBean
public class Logout {

    /**
     * Default constructor. 
     */
    	public String logout() throws IOException {
    	    ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
    	    ec.invalidateSession();
    	    return "/homePage.xhtml";
    	}
    

}
