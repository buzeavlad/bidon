/**
 * 
 */
package beans;

import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.*;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;




import daos.AuctionDAORemote;
import daos.AuctionParticipantDAORemote;
import dto.AuctionListDTO;
import dto.AuctionParticipantDTO;

/**
 * @author Vlad Buzea
 * ManagedBean that provides the data for populating myAuctions.xhtml and manages navigation to auction details page
 *
 */
@ManagedBean(name="myAuctions")
public class MyAuctions {
	
	/**
	 * User set on session in Login.
	 * Should be null only if not logged in.
	 */
	private AuctionParticipantDTO user;
	/**
	 * List of auctions that the user won
	 */
	private List<AuctionListDTO> wonAuctions;
	/**
	 * List of auctions that the user created
	 */
	private List<AuctionListDTO> createdAuctions;
	
	/**
	 * The SessionScoped ManagedBean that stores the logged in user
	 */
	@ManagedProperty(value="#{login}")
	private Login login;
	
	
	/**
	 * CRUD Operations implementation
	 */
	@EJB
	private AuctionDAORemote auctions;
	
	/**
	 * Business logic for AuctionParticipants
	 */
	@EJB
	private AuctionParticipantDAORemote userRemote;
	
	/**
	 * The logger
	 * Redundant
	 */
	private Logger LOGGER =Logger.getLogger(MyAuctions.class.getName());
	
	/**
	 * 
	 */
	public MyAuctions() {
		
	}
	/**
	 * Get the user from the session and initialize variables
	 */
	@PostConstruct
	public void init(){
		try{
		user=login.getAuctionParticipantDTO();
		wonAuctions = auctions.getWonAuctions(user);
		createdAuctions = auctions.getCreatedAuctions(user);
		/*
		//test with dummy auction
		wonAuctions=new LinkedList<AuctionDTOExpanded>();
		AuctionDTOExpanded temp = new AuctionDTOExpanded();
		temp.setProductName("Carne");
		temp.setDueDate(new Timestamp(System.currentTimeMillis()));
		temp.setPrice(1000);
		temp.setStartingPrice(100);
		AuctionParticipantDTO u=new AuctionParticipantDTO();
		u.setUsername("Vlad");
		temp.setSeller(u);
		wonAuctions.add(temp);
		createdAuctions=new LinkedList<AuctionDTOExpanded>();
		createdAuctions.add(temp);
		*/
		}catch (Exception e1){
			LOGGER.log(Level.INFO, "MyAuctions not initialized");
		}
	}
	
	
	/**
	 * @return the user
	 */
	public AuctionParticipantDTO getUser() {
		return user;
	}
	/**
	 * @param user the user to set
	 */
	public void setUser(AuctionParticipantDTO user) {
		this.user = user;
	}
	/**
	 * @return the wonAuctions
	 */
	public List<AuctionListDTO> getWonAuctions() {
		return wonAuctions;
	}
	/**
	 * @param wonAuctions the wonAuctions to set
	 */
	public void setWonAuctions(List<AuctionListDTO> wonAuctions) {
		this.wonAuctions = wonAuctions;
	}
	/**
	 * @return the createdAuctions
	 */
	public List<AuctionListDTO> getCreatedAuctions() {
		return createdAuctions;
	}
	/**
	 * @param createdAuctions the createdAuctions to set
	 */
	public void setCreatedAuctions(List<AuctionListDTO> createdAuctions) {
		this.createdAuctions = createdAuctions;
	}
	/**
	 * @return the login
	 */
	public Login getLogin() {
		return login;
	}
	/**
	 * @param login the login to set
	 */
	public void setLogin(Login login) {
		this.login = login;
	}
	
	
	
	/**
	 * Navigation function for auctions.
	 * @return the URL (without context) to access the Auction Details page of the selected Auction
	 */
	public String transmitAuction(){
		Map<String,String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		//int id = Integer.parseInt(params.get("id"));

		
		return "auctionDetails.xhtml?id="+params.get("id");
		
	}
	
	/**
	 * Navigation function for Users.
	 * @return the URL (without context) to access the view profile page of the selected AuctionParticipant
	 */
	public String transmitUser(){
		Map<String,String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		System.out.println(params.get("username"));
		return "viewProfile?username="+params.get("username"); 
	}
	
	/**
	 * Cancels the selected auction
	 * @return navigation rule back to myAuctions.xhtml
	 */
	public String cancelAuction(){
		Map<String,String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		int id = Integer.parseInt(params.get("idAuction"));
		System.out.println(id);
		if(auctions.cancelAuction(id))
			userRemote.applyPentalies(user.getUsername());
		
		createdAuctions=auctions.getCreatedAuctions(user);
		
		return "myAuctions.xhtml";
		
	}
	
	/**
	 * @return the auctions
	 */
	public AuctionDAORemote getAuctions() {
		return auctions;
	}
	/**
	 * @param auctions the auctions to set
	 */
	public void setAuctions(AuctionDAORemote auctions) {
		this.auctions = auctions;
	}
	/**
	 * @return the lOGGER
	 */
	public Logger getLOGGER() {
		return LOGGER;
	}
	/**
	 * @param lOGGER the lOGGER to set
	 */
	public void setLOGGER(Logger lOGGER) {
		LOGGER = lOGGER;
	}
	
	

}
