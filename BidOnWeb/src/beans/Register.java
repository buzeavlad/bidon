package beans;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.primefaces.event.FlowEvent;

import utils.PasswordEncryptor;
import utils.Validator;
import daos.AuctionParticipantDAORemote;
import dto.AuctionParticipantDTO;

/**
 * Managed bean that guides navigation from register form and creates a new
 * AuctionParticipant in the database.
 * 
 * @author Vlad Buzea, contributions added by Monica Barna and Carina Paul
 *
 */
@ManagedBean(name = "register")
@SessionScoped
public class Register {
	/**
	 * Field that holds the data for the new AuctionParticipant to be created in
	 * a DTO
	 */
	private AuctionParticipantDTO user;
	/**
	 * Field for reintroduced password
	 */
	private String rePass;

	/**
	 * Entity that creates user in DataBase
	 */
	@EJB
	private AuctionParticipantDAORemote aucPartDAO;

	/**
	 * Message to display in case of error when registering.
	 */
	private String message;

	/**
	 * Field used for AJAX
	 * 
	 * @author Carina
	 */
	private boolean displayPassword;

	/**
	 * Object that encrypts password
	 */
	private PasswordEncryptor passwordEncryptor;
	
	/**
	 * Fields used to create the user's address
	 */
	private String country, street, city, streetNo;
	
	/**
	 * Default Constructor
	 */
	public Register() {

	}

	/**
	 * Initialize fields
	 */
	@PostConstruct
	public void init() {
		user = new AuctionParticipantDTO();
		passwordEncryptor = new PasswordEncryptor();
	}

	/**
	 * @return the user
	 */
	public AuctionParticipantDTO getUser() {
		return user;
	}

	/**
	 * @param user
	 *            the user to set
	 */
	public void setUser(AuctionParticipantDTO user) {
		this.user = user;
	}

	/**
	 * @return the rePass
	 */
	public String getRePass() {
		return rePass;
	}

	/**
	 * @param rePass
	 *            the rePass to set
	 */
	public void setRePass(String rePass) {
		this.rePass = rePass;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the displayPassword
	 */
	public boolean isDisplayPassword() {
		return displayPassword;
	}

	/**
	 * @param displayPassword
	 *            the displayPassword to set
	 */
	public void setDisplayPassword(boolean displayPassword) {
		this.displayPassword = displayPassword;
	}

	public void displayChangePass() {
		displayPassword = !displayPassword;
	}

	/**
	 * Function used for navigation from register form
	 * 
	 * @return page navigation destination
	 */
	public String transmit() {
		try {user.setAddress(country+", "+city+", "+street+", "+streetNo);
			if (!user.getPassword().equals(rePass)) {
				message = "Passwords do not match!";
				return "register.xhtml";
			}
			user.setPassword(passwordEncryptor.encrypt(user.getPassword())); 	// encrypt
																				// password
			if (aucPartDAO.create(user)) 										// create user in the database
				return "homePage.xhtml";
		} catch (Exception e) {
			setMessage(e.getMessage());
		}

		return "register.xhtml";

	}
	/**
	 * @author Monica Barna
	 */
	public String onFlowProcess(FlowEvent event) {
		return event.getNewStep();
	}
	/**
	 * @author Monica Barna
	 */
	public void save() {
		FacesMessage msg = new FacesMessage("Successful", "Welcome :"
				+ user.getUsername());
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}


	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStreetNo() {
		return streetNo;
	}

	public void setStreetNo(String streetNo) {
		this.streetNo = streetNo;
	}

}
