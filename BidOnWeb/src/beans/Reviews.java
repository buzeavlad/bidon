package beans;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import dto.AuctionParticipantDTO;
import dto.ReviewDTO;
import daos.AuctionParticipantDAORemote;
import daos.ReviewDAORemote;

/**
 * 
 * @author Radu
 * provides reviews for the auctionParticipant in auctionDetails.xhtml
 */

@ManagedBean(name = "reviews")
@SessionScoped
public class Reviews {

	private int reviewNr;
	private String description;
	private int rating;
	private List<ReviewDTO> userReviews;
	/**
	 * User set on session in Login.
	 * Should be null only if not logged in.
	 */
	private AuctionParticipantDTO user;
	/**
	 * The SessionScoped ManagedBean that stores the logged in user
	 */
	@ManagedProperty(value="#{login}")
	private Login login;
	
	public Login getLogin() {
		return login;
	}


	public void setLogin(Login login) {
		this.login = login;
	}


	@EJB
	private ReviewDAORemote reviewDAORemote;
	
	@EJB
	private AuctionParticipantDAORemote auctionParticipantDAORemote;
	
	
	@PostConstruct
	public void init(){
		try{
		user=login.getAuctionParticipantDTO();
		userReviews = reviewDAORemote.findReviewByUser(auctionParticipantDAORemote.findIdByUsername(user.getUsername()));
		reviewNr = userReviews.size();
		}catch (Exception e1){
			e1.printStackTrace();
		}
	}
	
	
	/**
	 * @return the user
	 */
	public AuctionParticipantDTO getUser() {
		return user;
	}
	/**
	 * @param user the user to set
	 */
	public void setUser(AuctionParticipantDTO user) {
		this.user = user;
	}
	
	

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public List<ReviewDTO> getUserReviews() {
		return userReviews;
	}

	public void setUserReviews(List<ReviewDTO> userReviews) {
		this.userReviews = userReviews;
	}

	public int getReviewNr() {
		return reviewNr;
	}


	public void setReviewNr(int reviewNr) {
		this.reviewNr = reviewNr;
	}
	

}
