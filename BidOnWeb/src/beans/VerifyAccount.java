package beans;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.naming.NamingException;

import utils.VerifyAccountEJBRemote;
import daos.AuctionParticipantDAORemote;
import dto.AuctionParticipantDTO;

/**
 * This is the managed bean associated to AccountValidation.xhtml. It handles the
 * approval and the rejection of the newly created accounts and also sends the
 * list of accounts pending approval to the .xhtml file
 * 
 * @author marius.sutea
 *
 */

@ManagedBean(name = "verifyAccount")
public class VerifyAccount {
	public String email;
	public String firstName;
	public String lastName;
	public String username;

	private List<AuctionParticipantDTO> pendingAccounts;
	@EJB
	AuctionParticipantDAORemote auctionParticipantDAORemote;

	@EJB
	private VerifyAccountEJBRemote verifyAccountEJB;

	@PostConstruct
	private void init() {
		this.pendingAccounts = auctionParticipantDAORemote.allPendingAccounts();
		System.out.println(pendingAccounts);
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<AuctionParticipantDTO> getPendingAccounts() {
		return pendingAccounts;
	}

	public void setPendingAccounts(List<AuctionParticipantDTO> pendingAccounts) {
		this.pendingAccounts = pendingAccounts;
	}

	/**
	 * This method validates the account of the user
	 * 
	 * @return "accept"
	 * @throws NamingException
	 */
	public String transmitAccept() {
		Map<String, String> params = FacesContext.getCurrentInstance()
				.getExternalContext().getRequestParameterMap();
		String username = params.get("username");
		System.out.println(username);
		verifyAccountEJB.acceptAccount(username);
		this.pendingAccounts = auctionParticipantDAORemote.allPendingAccounts();

		return "AccountValidation.xhtml";
	}

	/**
	 * This method removes the user from the database after the administrator
	 * rejects his account.
	 * 
	 * @return "reject"
	 */
	public String transmitReject() {
		Map<String, String> params = FacesContext.getCurrentInstance()
				.getExternalContext().getRequestParameterMap();
		String username = params.get("username");

		verifyAccountEJB.reject(username);
		this.pendingAccounts = auctionParticipantDAORemote.allPendingAccounts();

		return "AccountValidation.xhtml";
	}
}
