package beans;

import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.event.ToggleEvent;

import daos.AuctionDAORemote;
import daos.AuctionParticipantDAORemote;
import daos.ReviewDAORemote;
import dto.AuctionParticipantDTO;
import dto.ReviewDTO;

@ManagedBean(name = "viewProfile")
public class ViewProfile {

	/**
	 * Parameter recieved from url (?username= value)
	 */
	@ManagedProperty(value = "#{param.username}")
	private String username;

	/**
	 * Value computed from all the ratings corresponding to the username
	 */
	private int ratingStars;
	
	/**
	 * Data correspoding to the username recieved as an DTO
	 */
	private AuctionParticipantDTO user;
	
	/**
	 * All the reviews corresponding to the user(name)
	 */
	private List<ReviewDTO> reviews;
	/**
	 * Business logic implementations for AuctionParticipant
	 */
	@EJB
	private AuctionParticipantDAORemote userDAO;

	/**
	 * Business logic implementations for Reviews
	 */
	@EJB
	private ReviewDAORemote reviewDAO;

	/**
	 * Business logic implementations for Auuctions
	 */
	@EJB
	private AuctionDAORemote auctionDAO;
	
	/**
	 * Flag if user is on his own profile and can edit it
	 */
	private boolean editable;

	/**
	 * The SessionScoped ManagedBean that stores the logged in user
	 */
	@ManagedProperty(value = "#{login}")
	private Login login;
	/**
	 * The logger. 
	 * Redundant information
	 */
	private Logger LOGGER = Logger.getLogger(ViewProfile.class.getName());

	public ViewProfile() {

	}

	@PostConstruct
	public void init() {
		try {
			editable=true;
			user = userDAO.findByUsername(username);
			AuctionParticipantDTO userLoggedIn = null;
			if (login != null)
				userLoggedIn = login.getAuctionParticipantDTO();

			if (userLoggedIn == null) {
				hideDetails();
				editable=false;
				LOGGER.log(Level.INFO,
						"No user logged in. No data shown in profile page.");
			} else {
				LOGGER.log(Level.INFO, userLoggedIn.getUsername()
						+ " is logged in");
				if (!userLoggedIn.getUsername().equals(username)) {
					user.setBidcoins(0);
					if (!auctionDAO.hasWonAuction(username,
							userLoggedIn.getUsername()))
						hideDetails();
					editable=false;
				}
				
			}
			reviews = reviewDAO.findReviewByUser(userDAO
					.findIdByUsername(username));
			int sum = 0, nr = 0;
			if (reviews != null)
				for (ReviewDTO i : reviews) {
					sum += i.getRating();
					nr++;
				}
			if (nr != 0)
				ratingStars = sum / nr;
			else
				ratingStars = 0;

		} catch (Exception e) {
			System.err.println("Exception caught in ViewProfile.java");
			if (user == null)
				user = new AuctionParticipantDTO("Unknown", "Unknown",
						"Unknown", "Unknown", "Unknown", "Unknown", -1,
						"Unknown", false,"INVALID");
			if (reviews == null) {
				reviews = new LinkedList<ReviewDTO>();
				ReviewDTO temp = new ReviewDTO();
				temp.setRating(0);
				temp.setDescription("NO REVIEWS");
				reviews.add(temp);
			}
		}

	}

	private void hideDetails() {
		user.setAddress("PRIVATE");
		user.setEmail("PRIVATE");
		user.setBidcoins(0);
		user.setPhone("PRIVATE");
		user.setFirstName("PRIVATE");
		user.setLastName("PRIVATE");
	}

	public void handleToggle(ToggleEvent event) {
		FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO,
				"Toggled", "Visibility:" + event.getVisibility());
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username
	 *            the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the ratingStars
	 */
	public int getRatingStars() {
		return ratingStars;
	}

	/**
	 * @param ratingStars
	 *            the ratingStars to set
	 */
	public void setRatingStars(int ratingStars) {
		this.ratingStars = ratingStars;
	}

	/**
	 * @return the user
	 */
	public AuctionParticipantDTO getUser() {
		return user;
	}

	/**
	 * @param user
	 *            the user to set
	 */
	public void setUser(AuctionParticipantDTO user) {
		this.user = user;
	}

	/**
	 * @return the reviews
	 */
	public List<ReviewDTO> getReviews() {
		return reviews;
	}

	/**
	 * @param reviews
	 *            the reviews to set
	 */
	public void setReviews(List<ReviewDTO> reviews) {
		this.reviews = reviews;
	}

	/**
	 * @return the userDAO
	 */
	public AuctionParticipantDAORemote getUserDAO() {
		return userDAO;
	}

	/**
	 * @param userDAO
	 *            the userDAO to set
	 */
	public void setUserDAO(AuctionParticipantDAORemote userDAO) {
		this.userDAO = userDAO;
	}

	/**
	 * @return the reviewDAO
	 */
	public ReviewDAORemote getReviewDAO() {
		return reviewDAO;
	}

	/**
	 * @param reviewDAO
	 *            the reviewDAO to set
	 */
	public void setReviewDAO(ReviewDAORemote reviewDAO) {
		this.reviewDAO = reviewDAO;
	}

	/**
	 * @return the login
	 */
	public Login getLogin() {
		return login;
	}

	/**
	 * @param login
	 *            the login to set
	 */
	public void setLogin(Login login) {
		this.login = login;
	}

	/**
	 * @return the editable
	 */
	public boolean isEditable() {
		return editable;
	}

	/**
	 * @param editable the editable to set
	 */
	public void setEditable(boolean editable) {
		this.editable = editable;
	}
	
	public String goToEditProfile(){
		
		return "editProfile.xhml";
	}

}
