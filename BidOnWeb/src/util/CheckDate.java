package util;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 * 
 * @author marius.sutea
 *
 */
@FacesValidator("checkDate")
public class CheckDate implements Validator {

	/**
	 * Checks that email address matches a predefined pattern and is unique in
	 * the database.
	 * 
	 * @param context
	 * @param component
	 * @param value
	 * @return void if date is valid.
	 * @throws ValidatorException
	 *             if date is not valid.
	 */
	@Override
	public void validate(FacesContext context, UIComponent component,
			Object value) throws ValidatorException {

		String date = (String) value;

		if (!date.matches("(^(((0[1-9]|[12][0-8])[/](0[1-9]|1[012]))|((29|30|31)[/](0[13578]|1[02]))|((29|30)[/](0[4,6,9]|11)))[/](19|[2-9][0-9])\\d\\d$)|(^29[/]02[/](19|[2-9][0-9])(00|04|08|12|16|20|24|28|32|36|40|44|48|52|56|60|64|68|72|76|80|84|88|92|96)$)")) {
			FacesMessage facesMessage = new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "Incorrect date format",
					"Incorrect date format");
			throw new ValidatorException(facesMessage);
		}
	}

}
