package util;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import daos.AuctionParticipantDAORemote;
import dto.AuctionParticipantDTO;

/**
 * 
 * @author Ildik�
 *
 */
@FacesValidator("checkEmail")
public class CheckEmail implements Validator {

	/**
	 * Checks that email address matches a predefined pattern and is unique in the database.
	 * @param context
	 * @param component
	 * @param value - an object that contains the email address as a String.
	 * @return void if email address is valid.
	 * @throws ValidatorException if email address is not valid.
	 */
	@Override
	public void validate(FacesContext context, UIComponent component,
			Object value) throws ValidatorException {

		String email = (String) value;

		if (!email.matches("(.+)@(.+)\\.(.+)")) {
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Email incorrect.", "Email address is incorrect.");
			throw new ValidatorException(message);
		}

		InitialContext icx;
		try {
			icx = new InitialContext();
			AuctionParticipantDAORemote apr = (AuctionParticipantDAORemote) icx
					.lookup("java:global/BidOnEAR/BidOnEJB/AuctionParticipantDAO!daos.AuctionParticipantDAORemote");

			AuctionParticipantDTO user = apr.findByEmail(email);

			if (user != null) {
				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Email already in use.", "Email address is already in use.");
				throw new ValidatorException(message);
			}

		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

}
