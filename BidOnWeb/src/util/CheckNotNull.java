package util;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

  
/**
 * 
 * @author Ildik�
 *
 */
@FacesValidator("checkNotNull")
public class CheckNotNull implements Validator{

	/**
	 * Checks that a string is not null and not an empty string or a string that contains only whitespaces.
	 * @param context
	 * @param component
	 * @param value - an object that contains a String.
	 * @return void if string is valid.
	 * @throws ValidatorException if string is not valid.
	 */
	@Override
	public void validate(FacesContext context, UIComponent component,
			Object value) throws ValidatorException {

		String string = (String) value;
		
		if (string == null || string.trim().equals("")){
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Field is null.", "Field cannot be empty.");
			
			throw new ValidatorException(message);
		}
	}

}
