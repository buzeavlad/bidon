package util;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;


/**
 * 
 * @author Ildiko
 *
 */
@FacesValidator("checkPassword")
public class CheckPassword implements Validator{

	/**
	 * Checks that password contains at least one digit, one lowercase and one uppercase character.
	 * @param context
	 * @param component
	 * @param value - an object that contains the password as String.
	 * @return void if password is valid.
	 * @throws ValidatorException if password is not valid.
	 */
	@Override
	public void validate(FacesContext context, UIComponent component,
			Object value) throws ValidatorException {
		
		String password = (String) value;
		
		if (!password.matches("(.*)\\d(.*)") || !password.matches("(.*)[a-z](.*)") || !password.matches("(.*)[A-Z](.*)")){
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Password incorrect.", "Password needs to contain at least one uppercase character, one lowercase character and one digit.");
			
			throw new ValidatorException(message);
		}
		
	}

}
