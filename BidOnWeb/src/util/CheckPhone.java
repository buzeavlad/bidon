package util;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 * 
 * @author Ildiko
 *
 */

@FacesValidator("checkPhone")
public class CheckPhone implements Validator{

	/**
	 * Checks that phone number contains only digits (at least one).
	 * @param context
	 * @param component
	 * @param value - an object that contains the phone number as a String.
	 * @return void if phone number is valid.
	 * @throws ValidatorException if phone number is not valid.
	 */
	@Override
	public void validate(FacesContext context, UIComponent component,
			Object value) throws ValidatorException {
		
		if (value==null)
			return;
		String phone = (String) value;
		// TODO + instead of *
		if (!phone.matches("\\d*")){
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Phone number incorrect.", "Phone number must contain only digits.");
			throw new ValidatorException(message);
		}
	}
	
	
}
