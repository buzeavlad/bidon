package util;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import daos.AuctionParticipantDAORemote;
import dto.AuctionParticipantDTO;

/**
 * 
 * @author Ildiko
 *
 */
@FacesValidator("checkUsername")
public class CheckUsername implements Validator{

	/**
	 * Checks that username is unique in the database.
	 * @param context
	 * @param component
	 * @param value - an object that contains the username as a String.
	 * @return void if username is valid.
	 * @throws ValidatorException if email username is not valid.
	 */
	@Override
	public void validate(FacesContext context, UIComponent component,
			Object value) throws ValidatorException {
		
		String username = (String) value;
		
		// TODO does not work, use logger to see
		InitialContext icx;
		try {
			icx = new InitialContext();
			AuctionParticipantDAORemote apr = (AuctionParticipantDAORemote) icx.lookup("java:global/BidOnEAR/BidOnEJB/AuctionParticipantDAO!daos.AuctionParticipantDAORemote");

			AuctionParticipantDTO user = apr.findByUsername(username);
			
			if (user!=null){ 
				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Username is taken.", "Username is taken.");
				throw new ValidatorException(message);
			}

		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

}
