package util;

public class Message {

	public static final String outBidError = "You were the last bidder. You can not outbid yourself!";
	private static final String bidCreated = "Bid has been succesfully created!";
	private static final String inssuficientBidCoins = "You don't have enough bidcoins!";
	private static final String bidderConflict = "You are the seller, you can not bid on this auction!";

	
	
	
	
	
	
	public static String getMessage(String message) {
		
		switch(message) {
		case "outBidError" : 
			return outBidError;
		case "bidCreated" :
			return bidCreated;
		case "insufficientBidCoins" :
			return inssuficientBidCoins;
		case "bidderConflict" :
			return bidderConflict;
		default: return "";
		}
	}
}
