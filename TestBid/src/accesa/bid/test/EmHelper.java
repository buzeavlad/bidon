package accesa.bid.test;

import java.util.*;

import javax.persistence.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 * EmHelper
 * 
 */
public class EmHelper {

	
	private static final Log log = LogFactory.getLog(EmHelper.class);
	/*
	 * Changing visibility to protected To allow wrapping EmHelper in a managed environment (Guice) we must allow access to the
	 * ThreadLocal otherwise we'll end up in two separate EMFs and ThreadLocals for EntityManagers
	 * 
	 * DO NOT ACCESS these fields for any other reason !
	 */
	protected static EntityManagerFactory EMF = null; // (single persistence unit) entweder ist das null
	protected static Map<String, EntityManagerFactory> EMFs = null; // (multiple persistence units) oder das

	// EntityManager des aktuellen Threads
	protected static final ThreadLocal<EntityManager> currentEntityManager = new ThreadLocal<EntityManager>();

	// nur multiple persistence units: Name der persistence unit zur Erzeugung eines EntityManagers
	protected static final ThreadLocal<String> currentPersistenceUnitName = new ThreadLocal<String>();

	/**
	 * Making constructor protected to allow subclassing Required to wrap EmHelper in Guice environment (ferenczil)
	 */
	protected EmHelper() {
		super();
	}

	/**
	 * @param persistenceUnitNames
	 */
	public static final synchronized void init(final List<String> persistenceUnitNames) {
		if (log.isDebugEnabled())
			log.debug("init mit units: " + persistenceUnitNames);

		if (persistenceUnitNames == null || persistenceUnitNames.size() == 0)
			throw new IllegalArgumentException("persistenceUnitNames sind leer: " + persistenceUnitNames);
		if (EMF != null || EMFs != null)
			throw new IllegalStateException("EMF(s) sind bereits initialisiert!");

		if (persistenceUnitNames.size() == 1) {
			// single persistence unit
			EMF = Persistence.createEntityManagerFactory(persistenceUnitNames.get(0));
		}
		else {
			// multiple persistence units
			EMFs = new HashMap<String, EntityManagerFactory>();
			for (final String unit : persistenceUnitNames) {
				if (EMFs.get(unit) != null)
					throw new IllegalStateException("EMF already initialized: " + unit);
				final EntityManagerFactory emf = Persistence.createEntityManagerFactory(unit);
				EMFs.put(unit, emf);
			}
		}

		// register shutdown hook
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@SuppressWarnings("synthetic-access")
			@Override
			public void run() {
				if (EMF != null) {
					// single persistence unit
					EMF.close();
					if (log.isDebugEnabled())
						log.debug("Closed single EntityManagerFactory");
				}

				if (EMFs != null) {
					// multiple persistence units
					for (final EntityManagerFactory emf : EMFs.values()) {
						if (emf == null)
							log.warn("EMF ist null?!?!?");
						else
							emf.close();
					}
					if (log.isDebugEnabled())
						log.debug("Closed EntityManagerFactories ");
				}
			}
		});
	}

	/**
	 * for convenience for single persistence unit projects
	 * 
	 * @param persistenceUnitName
	 */
	public static void init(final String persistenceUnitName) {
		final List<String> arr = new ArrayList<String>(1);
		arr.add(persistenceUnitName);
		init(arr);
	}

	public static boolean isInitialized() {
		return EMF != null || EMFs != null;
	}

	/**
	 * Lookup in {@link ThreadLocal} - if none there:
	 * <ul>
	 * <li>creates {@link EntityManager} and stores it in {@link ThreadLocal}
	 * <li>opens transaction
	 * </ul>
	 * 
	 * @return {@link EntityManager} with open transaction
	 */
	public static EntityManager get() {
		EntityManager em = currentEntityManager.get();
		if (em != null)
			return em;

		if (log.isTraceEnabled())
			log.trace("Setting EM");

		if (EMF != null) {
			// single persistence unit
			if (log.isTraceEnabled())
				log.trace("create single EM");
			em = EMF.createEntityManager();
		}
		else {
			if (log.isDebugEnabled())
				log.debug("create multiple EM");
			// multiple persistence units
			final String unit = currentPersistenceUnitName.get();
			if (unit == null)
				throw new IllegalStateException("name for (multiple) persistence unit is null!");
			final EntityManagerFactory emf = EMFs.get(unit);
			em = emf.createEntityManager();
		}
		currentEntityManager.set(em);
		em.getTransaction().begin();
		return em;
	}

	/**
	 * für Multi-Umgebungen
	 * 
	 * @param unit
	 * @return
	 */
	public static EntityManager get(final String unit) {
		setCurrentUnitName(unit);
		return get();
	}

	/**
	 * für Multi-Umgebungen<br>
	 * must be used at beginning of every thread when multiple persistence units are used
	 * 
	 * @param unit
	 */
	public static void setCurrentUnitName(final String unit) {
		if (log.isDebugEnabled())
			log.debug("set unit: " + unit);
		// alten closen
		close();
		currentPersistenceUnitName.set(unit);
	}

	/**
	 * für Multi-Umgebungen
	 * 
	 * @return current unit - use when starting a new thread
	 */
	public static String getCurrentUnitName() {
		return currentPersistenceUnitName.get();
	}

	/**
	 * Cleanup at end - never forget!
	 * <ul>
	 * <li>Lookup {@link EntityManager} in {@link ThreadLocal}
	 * <li>If there is one:
	 * <ul>
	 * <li>rollback open transactions
	 * <li>close {@link EntityManager}
	 * </ul>
	 * <li>unset {@link EntityManager} from {@link ThreadLocal}
	 * </ul>
	 */
	public static void close() {
		final EntityManager em = currentEntityManager.get();
		if (em != null) {
			final EntityTransaction transaction = em.getTransaction();
			if (transaction != null && transaction.isActive())
				try {
					transaction.rollback();
				}
				catch (final PersistenceException e) {
					log.error(e.getMessage(), e);
				}
			if (em.isOpen())
				em.close();
			currentEntityManager.set(null);
			if (log.isTraceEnabled())
				log.trace("UnSet EM");
		}

		// current unit name löschen bei multiple persistence units
		if (EMFs != null && currentPersistenceUnitName.get() != null)
			currentPersistenceUnitName.set(null);
	}

	public static void resetCurrentEntityManagerSilently() {
		final EntityManager em = currentEntityManager.get();
		if (em != null) {
			final EntityTransaction transaction = em.getTransaction();
			try {
				if (transaction != null && transaction.isActive())
					transaction.rollback();
			}
			catch (final Exception e) {
				// maybe connection already closed
			}
			try {
				if (em.isOpen())
					em.close();
			}
			catch (final Exception e) {
				// empty
			}
			currentEntityManager.set(null);
			if (log.isTraceEnabled())
				log.trace("UnSet EM (reset)");
		}
	}

	/**
	 * only for multi-persistence-environments
	 * 
	 * @return all unit names (or null in single persistence env.)
	 */
	public static final List<String> getPersistenceUnitNames() {
		return EMFs == null ? null : new ArrayList<String>(EMFs.keySet()); // just a copy
	}


}
